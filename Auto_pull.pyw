import keyboard,socket,time

global connexion_avec_serveur
hote = "192.168.1.134"
port = 12800
connexion_avec_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_avec_serveur.connect((hote, port))


def sendCommit():
    global connexion_avec_serveur
    msg_a_envoyer = "commit"
    msg_a_envoyer = msg_a_envoyer.encode()
    connexion_avec_serveur.send(msg_a_envoyer)
    time.sleep(2)
    return True

while True:  # making a loop
    try:  # used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('Ctrl + Asterisk'):  # if key 'q' is pressed 
            sendCommit()
            continue
    except:
        pass 
