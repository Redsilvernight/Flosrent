import socket,os,time
from datetime import datetime

def commit():
    os.system("git init")
    os.system("git remote -v")
    os.system("GIT_MERGE_AUTOEDIT=no")
    os.system("git config --global user.email 'redsilvernight@gmail.com'")
    os.system("git config --global user.name 'Redsilvernight'")
    os.system("git add *")
    os.system("git commit -m 'auto-pull'")
    os.system("git pull --no-edit origin master")
    os.system("sudo chgrp www-data *")
    os.system("sudo chmod a+r *")
    os.system("sudo chmod a+w *")
    os.system("sudo chmod a+x *")
    print("Pull to master at " + str(datetime.now()))
    return True



def loop():
    runLoop = True
    hote = ''
    port = 12800
    while runLoop:
        run = True
        connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connexion_principale.bind((hote, port))
        connexion_principale.listen(5)
        print("Le serveur écoute à présent sur le port {}".format(port))
        connexion_avec_client, infos_connexion = connexion_principale.accept()
        try:
            while run:
                msg_recu = b""
                while msg_recu != b"fin":
                    msg_recu = connexion_avec_client.recv(1024)
                    if(msg_recu.decode() == "commit"):
                        commit()
                        run = False
                    else:
                        continue
        except ConnectionResetError:
            print('Machine cliente déconnectée...')
            pass


    connexion_principale.close()


time.sleep(2)
loop()
