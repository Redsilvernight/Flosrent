<?php
session_start();
require('../../models/admin/model_add_user.php');
require('../../models/model_connection.php');
require('../../models/admin/model_add_product.php');

function lcdState($state)
{
    $fichier = fopen('../../publics/python/lcdState.txt','w');
    fputs($fichier,$state);
    fclose($fichier);
    return True;
}

function randomTag($table)
{
    $tag = strval(rand(1000,9999));

    if(testTag(sha1($tag),$table) == 1)
    {
        while(testTag(sha1($tag),$table) == 1)
        {
            $tag = intval($tag) + 1;
        }
    }
    
    return $tag;
}

function randomTagArticle()
{
    $tag_article = strval(rand(1000,9999));

    if(testTagArticle($_SESSION['tag'],$tag_article) == 1)
    {
        while(testTagArticle($_SESSION['tag'],$tag_article) == 1)
        {
            $tag_article = intval($tag) + 1;
        }
    }
    
    return $tag_article;
}




if(isset($_POST['form_connection']))
{
    if((!empty($_POST['prenom_inscription'])) and (!empty($_POST['nom_inscription'])) and (!empty($_POST['pseudoconnect'])) and (!empty($_POST['mdpconnect'])) and (!empty($_POST['status'])))
    {
        if(testLogin(array(htmlspecialchars($_POST['pseudoconnect']))) == 0)
        {
            $user = array(htmlspecialchars($_POST['prenom_inscription']),htmlspecialchars($_POST['nom_inscription']),htmlspecialchars($_POST['pseudoconnect']),htmlspecialchars($_POST['mdpconnect']),htmlspecialchars($_POST['status']));
            
            if(addUser($user))
            {
                header('Location: ../../vues/admin/add_tag.php?pseudo='.$_POST['pseudoconnect']);
            }
            else
            {
                header('Location: ../../vues/erreur.php?erreur=7');
            }
            
        }
        else
        {
            header('Location: ../../vues/erreur.php?erreur=6');
        }
        
    }
    else
    {
        header('Location: ../../vues/erreur.php?erreur=2');
    }
}

if(isset($_POST['btn_add_tag']))
{
    if(isset($_GET['name']) and (isset($_GET['tag'])))
    {
        $set_tag = setTag(htmlspecialchars($_GET['name']),$_GET['tag']);

        if($set_tag == TRUE)
        {
            lcdState(8);
            if(getStatus(sha1($_GET['tag'])) == "Client")
            {
                $tag = $_GET['tag'];
                exec('/usr/bin/python3 /var/www/html/Flosrent/publics/python/Scan.py setTag '.$tag.' client ');
                addClient(sha1($_GET['tag']));
                lcdState(9);
                header('Location: ../../routeur.php?page=4&tag='.$_SESSION['tag']);
            }
            elseif(getStatus(sha1($_GET['tag'])) == "Admin")
            {
                $tag = $_GET['tag'];
                exec('/usr/bin/python3 /var/www/html/Flosrent/publics/python/Scan.py setTag '.$tag.' admin ');
                addAdmin(sha1($_GET['tag']));
                header('Location: ../../routeur.php?page=4&tag='.$_SESSION['tag']);
            }
        }
    }
}

if(isset($_POST['btn_add_product']))
{
    if(!empty($_POST['product_name']) && !empty($_POST['product_description']) && !empty($_POST['product_price']) && !empty($_FILES['product_photo']) && intval($_POST['product_stock']) >= 0)
    {
        if(floatval($_POST['product_price']) && intval($_POST['product_stock']) >= 0)
        {
            if((pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "PNG") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "png") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "jpg" ) || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "JPG" ) || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "gif") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "GIF"))
            {
                $photo = $_FILES['product_photo']['name'];
                $dossier = "../../publics/Product_photos/";
                move_uploaded_file($_FILES['product_photo']['tmp_name'],$dossier.$_FILES['product_photo']['name']);
                addArticle($_SESSION['tag'],$_POST['product_name'],$_POST['product_description'],$_POST['product_price'],$_FILES['product_photo']['name'],$_POST['product_stock']);
                header('Location: ../../routeur.php?page=17&tag='.$_SESSION['tag'].'&product_name='.$_POST['product_name']);            }
            else
            {
                header('Location: ../../vues/erreur.php?erreur=16');
            }
        }
        else
        {
            header('Location: ../../vues/erreur.php?erreur=15');
        }
    }
    else
    {
        header('Location: ../../vues/erreur.php?erreur=2');
    }
}

if(isset($_POST['btn_add_tag_article']))
{
    if(isset($_GET['product_name']) and (isset($_GET['tag'])))
    {
        $set_tag_article = setTagArticle(htmlspecialchars($_GET['product_name']),$_GET['tag'],$_SESSION['tag']);

        if($set_tag_article == TRUE)
        {
            lcdState(8);
            exec('/usr/bin/python3 /var/www/html/Flosrent/publics/python/scan_produit.py setArticle flo20'.$_GET['tag']);
            lcdState(9);
            header('Location: ../../routeur.php?page=5&tag='.$_SESSION["tag"].'&success=1');
            
        }
    }
    
}

if(isset($_POST['btn_modify_product']))
{
    if(!empty($_FILES['product_photo']) && (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "PNG") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "png") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "jpg" ) || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "JPG" ) || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "gif") || (pathinfo($_FILES['product_photo']['name'],PATHINFO_EXTENSION) == "GIF"))
    {
        $photo = $_FILES['product_photo']['name'];
        $dossier = "../../publics/Product_photos/";
        move_uploaded_file($_FILES['product_photo']['tmp_name'],$dossier.$_FILES['product_photo']['name']);
        modifyPhotoArticle($_SESSION['tag'],$_GET['product'],$_FILES['product_photo']['name']);
        
    }
    
    if(!empty($_POST['product_name']) && !empty($_POST['product_description']) && !empty($_POST['product_price']) && intval($_POST['product_stock']) >= 0)
    {
        if(floatval($_POST['product_price']) && intval($_POST['product_stock']) >= 0)
        {
            modifyArticle($_SESSION['tag'],$_POST['product_name'],$_POST['product_description'],$_POST['product_price'],$_POST['product_stock'],$_GET['product']);
            header('Location: ../../routeur.php?page=5&tag='.$_SESSION['tag']); 
        }
        else
        {
            header('Location: ../../vues/erreur.php?erreur=15');
        }
    }
    else
    {
        header('Location: ../../vues/erreur.php?erreur=2');
    }
}
