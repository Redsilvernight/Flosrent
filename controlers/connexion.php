<?php
session_start();

require('../models/model_connection.php');

function lcdState($state)
{
    $fichier = fopen('../publics/python/lcdState.txt','w');
    fputs($fichier,$state);
    fclose($fichier);
    return True;
}

if (isset($_GET['scan']))
{
    lcdState(6);
    exec('/usr/bin/python3 /var/www/html/Flosrent/publics/python/Scan.py getTag',$scan_result);
      
    $tag = sha1(intval(substr($scan_result[2],5)));

    $tag_exist = testTag($tag,"flosrent_membres");

    if ($tag_exist == 1)
    {
        lcdState(7);
        $user_status = getStatus($tag);
        $_SESSION['tag'] = $tag;

        if($user_status == 'Admin')
        {
            header('Location: ../routeur.php?page=3&tag='.$tag);
        }
        elseif($user_status == 'Client')
        {
            header('Location: ../routeur.php?page=12&tag='.$tag);
        }
    }
    else
    {
        lcdState(3);
        header('Location: ../vues/erreur.php?erreur=1');
    }


   

    

    
}
else
{
    if ((!empty($_POST['pseudoconnect'])) AND (!empty($_POST['mdpconnect'])))
    {
        $login = array(htmlspecialchars($_POST['pseudoconnect']),htmlspecialchars($_POST['mdpconnect']));

        $user_exist = testLogin($login);
        
        if ($user_exist == 1)
        {

            $tag = getTag($login[0]);
            $mdp_user = getMdp($tag);

            if (sha1($login[1]) == $mdp_user)
            {
                $status = getStatus($tag);
                $_SESSION['tag'] = $tag;
                
                if($status == 'Admin')
                {
                    header('Location: ../routeur.php?page=3&tag='.$tag);
                }
                elseif($status == 'Client')
                {
                    header('Location: ../routeur.php?page=12');
                }
            }
            else
            {
                header('Location: ../vues/erreur.php?erreur=3');
            }
        }
        else
        {
            header('Location: ../vues/erreur.php?erreur=4');
        }
    }
    else
    {
        header('Location: ../vues/erreur.php?erreur=2');
    }
}



    
