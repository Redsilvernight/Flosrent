<?php

function setConfirm($type,$client="",$id="",$product="")
{
    switch($type)
	{
        case 1:
            return ["Etes-vous sûr de vouloir supprimer ce client ?","../controlers/admin/delete_client.php?client=".$client];
        break;
        case 2:
            return ["Etes-vous sûr de vouloir arrêter le passage en cours ? Aucun point ne sera gagner.","../controlers/admin/stop_passage.php?tag=".$_SESSION['tag']."&client=".$_GET['client']."&id=".$_GET['id']];
        break;
        case 3:
            return ["Etes-vous sûr de vouloir annulé ce rendez-vous ?","../controlers/admin/delete_rdv.php?tag=".$_SESSION['tag']."&id=".$id];
        break;
        case 4:
            return ["Etes-vous sûr de vouloir annulé ce rendez-vous ?","../controlers/user/delete_rdv.php?tag=".$_SESSION['tag']."&id=".$id];
        break;
        case 5:
            return ["Etes-vous sûr de vouloir supprimer ce produit ?","../controlers/admin/delete_product.php?tag=".$_SESSION['tag']."&product=".$product];
        break;
    }
}