<?php

function afficheErreur($erreur)
{
	switch($erreur)
	{
		case 1:
			return 'Numéro de badge inconnu.<br><br>Veuillez reessayer ou vous faire inscrire.';
		break;
		case 2:
			return 'Veuillez remplir tous les champs.';
		break;
		case 3:
			return 'Le mot de passe est incorect.';
		break;
		case 4:
			return "L'identifiant n'existe pas. Recommencez ou faites vous inscrire";
		break;
		case 5:
			return "Vous n'êtes pas connecté.";
		break;
		case 6:
			return "Ce pseudo est déjà utilisé. Veuillez en choisir un autre.";
		break;
		case 7:
			return "Une erreur c'est produite. Veuillez réessayez.";
		break;
		case 8:
			return "Jour complet. Impossible de prendre rendez-vous à cette date.";
		break;
		case 9:
			return "Vous devez prendre rendez-vous 24h à l'avance.";
		break;
		case 10:
			return "Vous ne pouvez pas prendre rendez-vous pour une date antérieure.";
		break;
		case 11:
			return "Vous ne pouvez pas avoir plus de trois rendez-vous en attentes.";
		break;
		case 12:
			return "Les nouveaux mots de passes doivent être identiques.";
		break;
		case 13:
			return "Le nouveau mot de passe doit être différent de l'ancient.";
		break;
		case 14:
			return "Votre mot de passe est incorrect.";
		break;
		case 15:
			return "Le prix doit être un chiffre entier ou décimal.";
		break;
		case 16:
			return "Extension du fichier photo non gérée. Utiliser jpeg, png ou gif.";
		break;
	}
}
