<?php
session_start();
require('../models/model_connection.php');

if(isset($_POST['change_password']))
{
    if(!empty($_POST['password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_password']))
    {
        if(sha1($_POST['password']) == getMdp($_SESSION['tag']))
        {
            if($_POST['new_password'] != $_POST['password'])
            {
                if($_POST['new_password'] == $_POST['confirm_password'])
                {
                    changePassword(sha1($_POST['new_password']), $_SESSION['tag']);
                    header('Location: ../routeur.php');

                }
                else
                {
                    header('Location: ../vues/erreur.php?erreur=12');
                }
            }
            else
            {
                header('Location: ../vues/erreur.php?erreur=13');
            }
        }
        else
        {
            header('Location: ../vues/erreur.php?erreur=14');
        }
    }
    else
    {
        header('Location: ../vues/erreur.php?erreur=2');
    }
    
}