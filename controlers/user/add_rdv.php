<?php
session_start();

require('../../models/user/model_user.php');

if (isset($_POST['btn_reservation']))
{
    if (!empty($_POST['inp_date'] && !empty($_POST['inp_time'])))
    {
        echo "bite";
        $date_rdv = $_POST['inp_date']." ".$_POST['inp_time'].":00";
        
        if (getRdvClient($date_rdv,$_SESSION['tag'])->rowCount() < 3)
        {
            if (getRdv(date('Y-m-d',strtotime($date_rdv)),$_SESSION['tag'])->rowCount() < 6)
            {
                $date_now = date('Y-m-d H:i:s');
                
                if (date('Y-m-d H:i:s',strtotime($date_rdv)) > date('Y-m-d H:i:s',strtotime($date_now.' + 1 hours')))
                {
                    if(date('Y-m-d H:i:s',strtotime($date_rdv)) > date('Y-m-d H:i:s',strtotime($date_now.' + 24 hours')).' '.date('H:M:S'))
                    {
                        setRdv($date_rdv,$_SESSION['tag']);
                        header('Location: ../../vues/user/reservation_user.php?tag='.$_SESSION['tag'].'&success');
                    }
                    else
                    {
                        header('Location: ../../vues/erreur.php?erreur=9');
                    }
                }
                else
                {
                    header('Location: ../../vues/erreur.php?erreur=10'); 
                }
            }
            else
            {
                header('Location: ../../vues/erreur.php?erreur=8');
            }
        }
        else
        {
            header('Location: ../../vues/erreur.php?erreur=11');
        }

        
    }
    else
    {
        header('Location: ../../vues/erreur.php?erreur=2');
    } 
}
