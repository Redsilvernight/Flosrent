<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/user/model_user.php');

if(testConnection($_GET['tag']) == TRUE)
{
    $points = getPoints($_SESSION['tag']);
    $article = getInfosArticle($_SESSION['tag'],$_GET['article'])->fetch();
    setAchat($_SESSION['tag'],$article);
    setPoints($_SESSION['tag'],floatval(floatval($points) - floatval($article['Prix'])));
    if(intval($article['Stock']) != 99)
    {
        setStock($_SESSION['tag'],$article['ID'],intval($article['Stock'] - 1));
    }
    header('Location: ../../vues/user/boutique_user.php?tag='.$_SESSION['tag'].'&success=1');
}
else
{
    header('Location: ../erreur.php?erreur=5');
}

