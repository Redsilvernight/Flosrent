<?php
require('../../models/user/model_user.php');

function calculPointsTotal($tag)
{
    $liste_point = getInfosTotalClient($tag);
    $points_total = 0;

    while($points = $liste_point->fetch())
    {
        $points_total += floatval($points['Points']);
    }
    
    return $points_total;
}

function calculTempsPasse($tag)
{
    $date = getInfosTotalClient($tag);
    $interval = 0;
    $interval_total = 0;
    while($passage = $date -> fetch())
    {
        if($passage['Date_sortie'] != "1111-11-11 11:11:11")
        {
            $entree = date("Y-m-d H:i:s",strtotime($passage['Date_entree']));
            $sortie = date("Y-m-d H:i:s",strtotime($passage['Date_sortie']));
            $interval += strtotime($sortie) - strtotime($entree);
            
        }
        
    }
    $temps = floor(($interval / 60) / 60);
    return $temps;
    
}