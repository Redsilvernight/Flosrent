<?php

function addArticle($tag,$name,$description,$price,$photo,$stock)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $add_article = $bdd -> prepare('INSERT INTO '.$table.' (Nom,Date_ajout,Prix,Lien_photo,Description,Stock) VALUES (?,?,?,?,?,?)');
    $add_article -> execute(array($name,date('Y,m,d'),$price,$photo,$description,$stock));

    return True;
}

function setTagArticle($name,$article_tag,$tag)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $set_tag = $bdd -> prepare('UPDATE '.$table.' SET Badge=? WHERE Nom=?');
    $set_tag -> execute(array($article_tag,$name));

    return TRUE;
}

function getInfosArticle($tag,$article)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $get_infos_article = $bdd -> query('SELECT * FROM `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83` WHERE ID='.$article);
    return $get_infos_article;
}

function modifyArticle($tag,$name,$description,$price,$stock,$id)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;
    $add_article = $bdd -> prepare('UPDATE '.$table.' SET Nom=?,Prix=?,Description=?,Stock=? WHERE ID=?');
    $add_article -> execute(array($name,$price,$description,$stock,$id));

    return TRUE;
}

function getAchatsId($tag,$article)
{
    $bdd = bddConnect("flosrent_achats");
    $table = "achats_".$tag;

    $get_achats_id = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Produit=? ORDER BY Date_achat DESC');
    $get_achats_id -> execute(array($article));

    return $get_achats_id;

}

function getClient($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_client = $bdd -> prepare('SELECT * FROM membres WHERE Badge=?');
    $get_client -> execute(array($tag));

    return $get_client;
}

function modifyPhotoArticle($tag,$article,$photo)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $modify_photo = $bdd -> prepare('UPDATE '.$table.' SET Lien_photo =? WHERE ID =?');
    $modify_photo -> execute(array($photo,$article));
    return TRUE;

}