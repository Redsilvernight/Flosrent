<?php
function addUser($user)
{
    $bdd = bddConnect('flosrent_membres');

    $insert_user = $bdd -> prepare('INSERT INTO membres (Prenom,Nom,Pseudo,Mdp,Badge,Status,Date_inscription,Id_admin) VALUES (?,?,?,?,?,?,?,?)');
    $insert_user -> execute(array($user[0],$user[1],$user[2],sha1($user[3]),0,$user[4],date('Y-m-d'),null));
    return TRUE;
}

function setTag($user,$tag)
{
    $bdd = bddConnect('flosrent_membres');

    $set_tag = $bdd -> prepare('UPDATE membres SET Badge=? WHERE Pseudo=?');
    $set_tag -> execute(array(sha1($tag),$user));

    return TRUE;
}

function addClient($tag)
{
    $bdd = bddConnect('flosrent_clients');
    $bdd_membres = bddConnect('flosrent_membres');

    $table_name = "clients_".$_SESSION['tag'];

    $add_client = $bdd -> prepare('INSERT INTO '.$table_name.' (Badge,Points,Date_rdv,Heure_rdv,Id_passage) VALUES (?,?,?,?,?)');
    $add_client -> execute(array($tag,0,null,null,null));
    
    $get_id_admin = $bdd_membres -> prepare('SELECT * from membres WHERE Badge=?');
    $get_id_admin -> execute(array($_SESSION['tag']));
    $id_admin = $get_id_admin->fetch();
    
    $set_admin = $bdd_membres -> prepare('UPDATE membres set Id_admin=? WHERE Badge=?');
    $set_admin -> execute(array($id_admin['ID'],$tag));

    return TRUE;
}

function addAdmin($tag)
{
    $bdd_achats = bddConnect('flosrent_achats');
    $achat = "achats_".$tag;
    $bdd_boutique = bddConnect('flosrent_boutique');
    $boutique = "boutique_".$tag;
    $bdd_passages = bddConnect('flosrent_passages');
    $passage = "passages_".$tag;
    $bdd_rdv = bddConnect('flosrent_rdv');
    $rdv = "rdv_".$tag;
    $bdd_clients = bddConnect('flosrent_clients');
    $clients = "clients_".$tag;

    $create_achats = $bdd_achats -> query('CREATE TABLE '.$achat.' (ID INT(255) AUTO_INCREMENT PRIMARY KEY,
                                                                        Badge VARCHAR(255) NULL,
                                                                        Produit VARCHAR(255) NULL,
                                                                        Date DATETIME NULL)');

    $create_boutique = $bdd_boutique -> query('CREATE TABLE '.$boutique.' (ID INT(255) AUTO_INCREMENT PRIMARY KEY,
                                                                            Nom VARCHAR(255) NULL,
                                                                            Date DATETIME NULL,
                                                                            Prix INT(255) NULL,
                                                                            Photo VARCHAR(255) NULL,
                                                                            Code VARCHAR(255) NULL)');

    $create_passages = $bdd_passages -> query('CREATE TABLE '.$passage.' (ID INT(255) AUTO_INCREMENT PRIMARY KEY,
                                                                            Badge VARCHAR(255) NULL,
                                                                            Date_entree DATETIME NULL,
                                                                            Points FLOAT NULL,
                                                                            Id_passage INT(255),
                                                                            Date_sortie DATETIME NULL)');

    $create_rdv = $bdd_rdv -> query('CREATE TABLE '.$rdv.' (ID INT(255) AUTO_INCREMENT PRIMARY KEY,
                                                            Badge VARCHAR(255) NULL,
                                                            Date_rdv DATETIME NULL,
                                                            Etat INT NULL)');                                                                       
                                                                        
    $create_client = $bdd_clients -> query('CREATE TABLE '.$clients.' (ID INT(255) AUTO_INCREMENT PRIMARY KEY,
                                                            Badge VARCHAR(255) NULL,
                                                            Points FLOAT NULL,
                                                            Date_rdv DATE NULL,
                                                            Heure_rdv TIME NULL,
                                                            Id_passage INT(255) NULL)');
    return;
}
