<?php
require('../../models/model_connection.php');

function getClientToClient($tag)
{
    $bdd = bddConnect("flosrent_clients");
    $table_client = "clients_".$tag;

    $get_client = $bdd -> query('SELECT * FROM '.$table_client);

    return $get_client;

}

function getClientToMembre($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_client = $bdd -> prepare('SELECT * FROM membres WHERE Badge=?');
    $get_client -> execute(array($tag));

    return $get_client;
}

function getPseudo($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_pseudo = $bdd -> prepare('SELECT Pseudo FROM membres WHERE Badge=?');
    $get_pseudo->execute(array($tag));
    $pseudo = $get_pseudo->fetch();

    return ucfirst($pseudo['Pseudo']);
}

function getPassages($tag)
{
    $bdd = bddConnect("flosrent_passages");
    $table_passage = "passages_".$tag;

    $get_passage = $bdd -> query('SELECT * FROM '.$table_passage.' ORDER BY Date_entree DESC');

    return $get_passage;
}

function getInfosClient($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_infos = $bdd -> prepare('SELECT * FROM membres WHERE Badge=?');
    $get_infos->execute(array($tag));
    return $get_infos;
}

function deleteAchat($tag,$client)
{
    $bdd = bddConnect("flosrent_achats");
    $table = "achats_".$tag;

    $delete_client = $bdd -> query('DELETE FROM '.$table.' WHERE tag= '.$client);
}

function deleteClient($tag,$client)
{
    $bdd = bddConnect("flosrent_membres");

    $delete_client = $bdd -> prepare('DELETE FROM membres WHERE Badge = ?');
    $delete_client -> execute(array($client));

    return TRUE;
}

function deleteRdv($tag,$client)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".$tag;

    $delete_client = $bdd -> prepare('DELETE FROM '.$table.' WHERE Badge = ?');
    $delete_client -> execute(array($client));

    return TRUE;
}

function deletePassage($tag,$client)
{
    $bdd = bddConnect("flosrent_passages");
    $table = "passages_".$tag;

    $delete_client = $bdd -> prepare('DELETE FROM '.$table.' WHERE Badge = ?');
    $delete_client -> execute(array($client));

    return TRUE;
}

function getArticle($tag)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $get_article = $bdd -> query('SELECT * FROM '.$table);

    return $get_article;
}