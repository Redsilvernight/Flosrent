<?php
require('../../models/model_connection.php');

function getInfosClient($id)
{
    $bdd = bddConnect("flosrent_clients");
    $table = "clients_".$_SESSION['tag'];

    $get_clients = $bdd -> query('SELECT * FROM '.$table.' WHERE ID='.$id);
    return $get_clients;
}

function getInfosMembres($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_membres = $bdd -> prepare('SELECT * FROM membres WHERE Badge=?');
    $get_membres -> execute(array($tag));

    return $get_membres;
}

function stopId($tag,$client)
{
    $bdd = bddConnect("flosrent_clients");
    $table = "clients_".$tag;


    $set_id = $bdd -> prepare('UPDATE '.$table.' SET Id_passage = NULL WHERE Badge = ?');
    $set_id ->execute(array($client));

    return TRUE;
}

function stopPassage($id,$tag,$client)
{
    $bdd = bddConnect("flosrent_passages");
    $table = "passages_".$tag;


    $set_id = $bdd -> prepare('UPDATE '.$table.' SET Date_sortie = ? WHERE Badge = ? AND Id_passage =?');
    $set_id ->execute(array("1111-11-11 11:11:11",$client, $id));

    return TRUE;
}

function getPassages($tag, $client)
{
    $bdd = bddConnect("flosrent_passages");
    $table = "passages_".$tag;

    $get_passage = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge = ? ORDER BY Date_entree DESC');
    $get_passage->execute(array($client));

    return $get_passage;
}

function getRdv($tag, $client)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".$tag;

    $get_rdv = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge = ? ORDER BY Date_rdv DESC');
    $get_rdv -> execute(array($client));

    return $get_rdv;
}

function cancelRdv($tag, $id)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".$tag;

    $delete_rdv = $bdd -> prepare('UPDATE '.$table.' SET Etat = ? WHERE id = ?');
    $delete_rdv -> execute(array(3,$id));

    return TRUE;
}

function getAchats($client,$tag)
{
    $bdd = bddConnect("flosrent_achats");
    $table = "achats_".$tag;

    $get_achats = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge = ? ORDER BY Date_achat DESC');
    $get_achats -> execute(array($client));

    return $get_achats;
}

function getAchatsArticle($id,$tag)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $get_article = $bdd -> prepare('SELECT * FROM '.$table.' WHERE ID = ? ');
    $get_article -> execute(array($id));

    return $get_article; 
}

function deleteProduct($tag,$product)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".$tag;

    $delete_product = $bdd -> prepare('DELETE FROM '.$table.' WHERE ID=?');
    $delete_product -> execute(array($product));

    return TRUE;
}