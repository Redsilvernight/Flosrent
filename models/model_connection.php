<?php
function bddConnect($dbname)
{
    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname='.$dbname ,"root","roule1grosboze");
        return $bdd;
    }
    catch(Exception $e)
    {
        die ('Erreur : ' .$e -> getMessage());
    }
}

function testLogin($login)
{ 
    $bdd = bddConnect('flosrent_membres');

    $verif_user = $bdd->prepare('SELECT * FROM membres WHERE Pseudo=?');
    $verif_user->execute(array($login[0]));

    $user_exist = $verif_user -> rowCount();

    return $user_exist;
}

function testTag($tag,$table)
{
    $bdd = bddConnect('flosrent_membres');

    $verif_tag = $bdd->prepare('SELECT * FROM membres WHERE Badge=?');
    $verif_tag -> execute(array($tag));

    $tag_exist = $verif_tag->rowCount();
    echo $tag;
    return $tag_exist;
}

function testTagArticle($tag,$article)
{
    $bdd = bddConnect('flosrent_boutique');
    $table = "boutique_".$tag;

    $verif_tag = $bdd->prepare('SELECT * FROM '.$table.' WHERE Badge=?');
    $verif_tag -> execute(array($article));

    $tag_exist = $verif_tag->rowCount();
    return $tag_exist;
}

function getTag($login)
{
    $bdd = bddConnect('flosrent_membres');

    $get_tag = $bdd -> prepare('SELECT * FROM membres WHERE Pseudo=? ');
    $get_tag -> execute(array($login));

    $user_tag = $get_tag->fetch();
    return $user_tag['Badge'];
}

function getMdp($tag)
{
    $bdd = bddConnect('flosrent_membres');

    $get_mdp = $bdd -> prepare('SELECT * FROM membres WHERE Badge=? ');
    $get_mdp -> execute(array($tag));

    $user_mdp = $get_mdp->fetch();
    return $user_mdp['Mdp'];

}

function getStatus($tag)
{
    $bdd = bddConnect('flosrent_membres');

    $status_user = $bdd->prepare('SELECT * FROM membres WHERE Badge=?');
    $status_user->execute(array($tag));

    $status_user = $status_user->fetch();
    return $status_user['Status'];
}

function changePassword($password, $tag)
{
    $bdd = bddConnect("flosrent_membres");

    $update_password = $bdd -> prepare('UPDATE membres SET Mdp = ? WHERE Badge = ?');
    $update_password->execute(array($password,$tag));

    return TRUE;
}
