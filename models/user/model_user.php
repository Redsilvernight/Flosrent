<?php
require('../../models/model_connection.php');

function getPseudo($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_pseudo = $bdd -> prepare('SELECT Pseudo FROM membres WHERE Badge=?');
    $get_pseudo->execute(array($tag));
    $pseudo = $get_pseudo->fetch();

    return ucfirst($pseudo['Pseudo']);
}

function getAdmin($tag)
{
    $bdd = bddConnect("flosrent_membres");

    $get_id_admin = $bdd -> prepare('SELECT Id_admin FROM membres WHERE BADGE=?');
    $get_id_admin -> execute(array($tag));

    $id_admin = $get_id_admin-> fetch();
    return $id_admin;
}

function getTagAdmin($tag)
{
    $bdd = bddConnect("flosrent_membres");
 
    $get_tag_admin = $bdd -> query('SELECT Badge FROM membres WHERE ID='.intval(getAdmin($tag)));
    $tag_admin = $get_tag_admin->fetch();

    return $tag_admin['Badge'];
}

function getPoints($tag)
{
    $bdd = bddConnect("flosrent_clients");
    $table = "clients_".getTagAdmin($tag);
    
    $get_points = $bdd -> prepare('SELECT Points FROM '.$table.' WHERE Badge=?');
    $get_points->execute(array($tag));
    $points = $get_points->fetch();
    return $points['Points'];
}

function getPassages($tag)
{
    $bdd = bddConnect("flosrent_passages");
    $table = "passages_".getTagAdmin($tag);

    $get_passages = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge=? ORDER BY Date_entree DESC');
    $get_passages->execute(array($tag));

    return $get_passages;
}

function setRdv($rdv,$tag)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".getTagAdmin($tag);

    $set_rdv = $bdd -> prepare('INSERT INTO '.$table.' (Badge,Date_rdv) VALUES (?,?)');
    $set_rdv -> execute(array($tag,$rdv));
    
    return '';

}

function getRdv($rdv,$tag)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".getTagAdmin($tag);
    $date_debut = $rdv.' 00:00:00';
    $date_fin = $rdv.' 23:59:59';
    $get_rdv = $bdd -> prepare('SELECT * from '.$table.' WHERE Date_rdv BETWEEN ? AND ?');
    $get_rdv -> execute(array($date_debut,$date_fin));

    return $get_rdv;
}

function getRdvClient($rdv,$tag)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".getTagAdmin($tag);
    $start_date = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').'+ 1 hours'));
    $get_rdv_client = $bdd -> prepare('SELECT * from '.$table.' WHERE Badge=? AND Date_rdv > ? AND Etat IS NULL');
    $get_rdv_client -> execute(array($tag,$start_date));

    return $get_rdv_client;
}

function getAllRdv($tag)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".getTagAdmin($tag);
    
    $get_rdv = $bdd ->prepare('SELECT * FROM '.$table.' WHERE Badge = ? AND Etat IS NULL');
    $get_rdv -> execute(array($tag));
    
    return $get_rdv;
}

function cancelRdv($tag, $id)
{
    $bdd = bddConnect("flosrent_rdv");
    $table = "rdv_".getTagAdmin($tag);

    $cancel_rdv = $bdd -> prepare('UPDATE '.$table.' SET Etat = ? WHERE id = ?');
    $cancel_rdv -> execute(array(3, $id));

    return TRUE;
}

function getInfosTotalClient($tag)
{
    $bdd = bddConnect("flosrent_passages");
    $table = "passages_".getTagAdmin($tag);

    $get_points = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge = ?');
    $get_points -> execute(array($tag));

    return $get_points;
}

function getArticle($tag)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".getTagAdmin($tag);

    $get_article = $bdd -> query('SELECT * FROM '.$table);

    return $get_article;
}

function getInfosArticle($tag,$article)
{
    $bdd = bddConnect('flosrent_boutique');
    $table = "boutique_".getTagAdmin($tag);

    $get_infos = $bdd -> prepare('SELECT * FROM '.$table.' WHERE ID=?');
    $get_infos -> execute(array($article));

    return $get_infos;

}

function setAchat($tag,$article)
{
    $bdd = bddConnect("flosrent_achats");
    $table = "achats_".getTagAdmin($tag);

    $set_achat = $bdd -> prepare('INSERT INTO '.$table.' (Badge,Produit,Date_achat) VALUES (?,?,?)');
    $set_achat -> execute(array($tag,$article['ID'],date('Y-m-d')));

    return TRUE;
}

function setPoints($tag, $points)
{
    $bdd = bddConnect("flosrent_clients");
    $table = "clients_".getTagAdmin($tag);

    $set_points = $bdd -> prepare('UPDATE '.$table.' SET Points = ? WHERE Badge = ? ');
    $set_points -> execute(array($points,$tag));

    return TRUE;
}

function setStock($tag, $produit, $stock)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".getTagAdmin($tag);

    $set_stock = $bdd -> prepare('UPDATE '.$table.' SET Stock = ? WHERE ID = ?');
    $set_stock -> execute(array($stock, $produit));

    return TRUE;
}

function getAchats($tag)
{
    $bdd = bddConnect("flosrent_achats");
    $table = "achats_".getTagAdmin($tag);

    $get_achats = $bdd -> prepare('SELECT * FROM '.$table.' WHERE Badge = ?');
    $get_achats -> execute(array($tag));

    return $get_achats;
}

function getAchatsArticle($id,$tag)
{
    $bdd = bddConnect("flosrent_boutique");
    $table = "boutique_".getTagAdmin($tag);

    $get_article = $bdd -> prepare('SELECT * FROM '.$table.' WHERE ID = ?');
    $get_article -> execute(array($id));

    return $get_article;
}