-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 26 Avril 2020 à 11:03
-- Version du serveur :  10.3.22-MariaDB-0+deb10u1
-- Version de PHP :  7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flosrent_boutique`
--

-- --------------------------------------------------------

--
-- Structure de la table `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

CREATE TABLE `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83` (
  `ID` int(255) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Date_ajout` date NOT NULL,
  `Prix` int(255) NOT NULL,
  `Lien_photo` varchar(255) NOT NULL,
  `Description` mediumtext NOT NULL,
  `Stock` int(11) NOT NULL,
  `Badge` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

INSERT INTO `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83` (`ID`, `Nom`, `Date_ajout`, `Prix`, `Lien_photo`, `Description`, `Stock`, `Badge`) VALUES
(6, 'Un verre d\'eau', '2020-03-29', 3, 'unnamed.png', 'C\'est un banal verre d\'eau. La taille du verre est aléatoire. Il peut être resservi autant de fois que nécessaire durant le passage.', 99, 3198),
(7, 'Une cigarette roulée', '2020-03-29', 5, 'cigarrette.png', 'Banale cigarette, probablement composée de tabac Camel.', 6, 5096),
(8, 'Morceau de carton', '2020-03-29', 2, 'raw-rolling-paper-filter-tips.png', 'Un morceau de carton quelconque pour une utilisation au choix.', 99, 1421),
(9, 'Une fin de joint acceptable', '2020-03-29', 18, '91157348_311825003116702_2882577698826223616_n.jpg', 'La taille est aléatoire. Il vaut mieux voir le joint a moitié plein.', 0, 6012),
(23, 'Un rangement de substitution', '2020-04-03', 7, '91633194_241377647252541_1925682556057944064_n.jpg', 'Marre de perdre vos affaires ? Il est l\'homme de la situation.', 8, 7490);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `boutique_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
