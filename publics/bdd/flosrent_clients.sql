-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 26 Avril 2020 à 11:04
-- Version du serveur :  10.3.22-MariaDB-0+deb10u1
-- Version de PHP :  7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flosrent_clients`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

CREATE TABLE `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83` (
  `ID` int(255) NOT NULL,
  `Badge` varchar(255) NOT NULL,
  `Points` float NOT NULL,
  `Date_rdv` date DEFAULT NULL,
  `Heure_rdv` time DEFAULT NULL,
  `Id_passage` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

INSERT INTO `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83` (`ID`, `Badge`, `Points`, `Date_rdv`, `Heure_rdv`, `Id_passage`) VALUES
(20, '02f84308fab673d8332e1ab780a8ade20987e925', 2.2, NULL, NULL, NULL),
(21, '71b43e87a1c83ad7cd80d45786feece1892e6ff7', 82.59, NULL, NULL, NULL),
(22, 'f9922496ec864c9b125d22f29bae753da6b52e17', 0.12, NULL, NULL, NULL),
(23, 'af017a7bbba51f62017449da83302dca33dabbb0', 53.28, NULL, NULL, NULL),
(24, '73e7214f002d370debf39ca0f8eb60cca7fe021f', 237.74, NULL, NULL, NULL),
(30, '415d2e4353963c539961c46ad289844f4093ba96', 131.15, NULL, NULL, NULL),
(31, 'ce50b60c685ba31a1d359a5c68bce9c521fcded2', 41.13, NULL, NULL, NULL),
(32, '49528c78ab65b909a799fa01d7210a50e735d0d3', 9813.32, NULL, NULL, NULL),
(33, '54fe9f1db595baf4d0ed76d647cb931d3e4ec936', 0, NULL, NULL, NULL),
(34, '97bccac39e344b46f9925805a86ba112ddb90d36', 0.25, NULL, NULL, NULL),
(36, '04c3eb5ce6c5e299ad93dac871bbbed16da09e21', 0.18, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `clients_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
