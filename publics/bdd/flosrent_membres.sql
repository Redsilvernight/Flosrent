-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 26 Avril 2020 à 11:04
-- Version du serveur :  10.3.22-MariaDB-0+deb10u1
-- Version de PHP :  7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flosrent_membres`
--

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE `membres` (
  `ID` int(255) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Pseudo` varchar(255) NOT NULL,
  `Mdp` varchar(255) NOT NULL,
  `Badge` varchar(255) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `Date_inscription` date NOT NULL,
  `Id_admin` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`ID`, `Nom`, `Prenom`, `Pseudo`, `Mdp`, `Badge`, `Status`, `Date_inscription`, `Id_admin`) VALUES
(1, 'sabio', 'florent', 'redsilvernight', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '47c5c6f332aa2d927a70073a43090d99c39fcd83', 'Admin', '2020-01-30', NULL),
(30, 'boisson', 'marie', 'crabe', '40cda6917d89a0c40c29a0b45ecba6a653c2de3b', '02f84308fab673d8332e1ab780a8ade20987e925', 'Client', '2020-02-14', 1),
(31, 'Cantogrel', 'Mathieu', 'mcanto', '5423181a164ce536133dad60b73a999129a9254e', '71b43e87a1c83ad7cd80d45786feece1892e6ff7', 'Client', '2020-02-15', 1),
(36, 'Martin', 'Laetitia', 'titi', 'd611d3160006dd2192495446cdcf374b92b76c89', 'f9922496ec864c9b125d22f29bae753da6b52e17', 'Client', '2020-02-16', 1),
(37, 'abbas', 'joris', 'LeGrandJojo', '7acdffef448d0116adcc5c51b7153fc1eb66a63b', 'af017a7bbba51f62017449da83302dca33dabbb0', 'Client', '2020-02-17', 1),
(38, 'cheniguer', 'merwan', 'exp626', '338d27f3ddf7f2b0e13bcb5bf842a0eb945e23e1', '73e7214f002d370debf39ca0f8eb60cca7fe021f', 'Client', '2020-02-26', 1),
(46, 'hocde', 'alexandre', 'passpecialement', '915c4e42ac9169c0b96a0162e92ab4b960345390', 'ce50b60c685ba31a1d359a5c68bce9c521fcded2', 'Client', '2020-02-29', 1),
(47, 'test', 'test', 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '49528c78ab65b909a799fa01d7210a50e735d0d3', 'Client', '2020-03-17', 1),
(50, 'Terrier', 'Natacha', 'natouuwww', '1d54185407196960add26b6b7ebd58f5095361da', '54fe9f1db595baf4d0ed76d647cb931d3e4ec936', 'Client', '2020-04-02', 1),
(53, 'Martin', 'Evan', 'evan', '95e9f231ea22e79b25a9471370ca3ce32cdeb108', '04c3eb5ce6c5e299ad93dac871bbbed16da09e21', 'Client', '2020-04-12', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `membres`
--
ALTER TABLE `membres`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `membres`
--
ALTER TABLE `membres`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
