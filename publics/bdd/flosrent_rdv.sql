-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 26 Avril 2020 à 11:05
-- Version du serveur :  10.3.22-MariaDB-0+deb10u1
-- Version de PHP :  7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flosrent_rdv`
--

-- --------------------------------------------------------

--
-- Structure de la table `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

CREATE TABLE `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83` (
  `id` int(255) NOT NULL,
  `Badge` varchar(255) NOT NULL,
  `Date_rdv` datetime NOT NULL,
  `Etat` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--

INSERT INTO `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83` (`id`, `Badge`, `Date_rdv`, `Etat`) VALUES
(82, '49528c78ab65b909a799fa01d7210a50e735d0d3', '2020-03-31 16:20:00', 3),
(83, '73e7214f002d370debf39ca0f8eb60cca7fe021f', '2020-03-30 14:15:00', 2),
(80, '49528c78ab65b909a799fa01d7210a50e735d0d3', '2020-03-20 16:20:00', 3),
(81, '73e7214f002d370debf39ca0f8eb60cca7fe021f', '2020-03-29 14:01:00', 3),
(66, 'af017a7bbba51f62017449da83302dca33dabbb0', '2020-03-05 10:00:00', 2),
(78, '49528c78ab65b909a799fa01d7210a50e735d0d3', '2020-03-18 11:32:00', 2),
(79, '49528c78ab65b909a799fa01d7210a50e735d0d3', '2020-03-19 16:20:00', 3),
(84, '04c3eb5ce6c5e299ad93dac871bbbed16da09e21', '2020-04-13 20:15:00', 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
--
ALTER TABLE `rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
