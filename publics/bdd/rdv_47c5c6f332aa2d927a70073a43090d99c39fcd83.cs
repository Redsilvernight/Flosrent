using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Flosrent_rdv
{
    #region Rdv_ccfaadaadcfcd
    public class Rdv_ccfaadaadcfcd
    {
        #region Member Variables
        protected int _id;
        protected string _Badge;
        protected DateTime _Date_rdv;
        protected int _Etat;
        #endregion
        #region Constructors
        public Rdv_ccfaadaadcfcd() { }
        public Rdv_ccfaadaadcfcd(string Badge, DateTime Date_rdv, int Etat)
        {
            this._Badge=Badge;
            this._Date_rdv=Date_rdv;
            this._Etat=Etat;
        }
        #endregion
        #region Public Properties
        public virtual int Id
        {
            get {return _id;}
            set {_id=value;}
        }
        public virtual string Badge
        {
            get {return _Badge;}
            set {_Badge=value;}
        }
        public virtual DateTime Date_rdv
        {
            get {return _Date_rdv;}
            set {_Date_rdv=value;}
        }
        public virtual int Etat
        {
            get {return _Etat;}
            set {_Etat=value;}
        }
        #endregion
    }
    #endregion
}