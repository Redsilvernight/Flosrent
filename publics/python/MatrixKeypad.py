#!/usr/bin/env python3
########################################################################
# Filename    : MatrixKeypad.py
# Description : obtain the key code of 4x4 Matrix Keypad
# Author      : freenove
# modification: 2018/08/03
########################################################################
import RPi.GPIO as GPIO
from Scan import *
from lcdTest import writeState
from model_passage import *
from model_boutique import articleExist
import Keypad
import hashlib
from boutique import setBoutique,traitementPanier
from threading import Thread

class detectEnd(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.end_key=0

    def run(self):
        global keystop
        print("Started...")
        keypad = Keypad.Keypad(keys,rowsPins,colsPins,ROWS,COLS)    #creat Keypad object
        keypad.setDebounceTime(50)      #set the debounce time

        while(True):
            key = keypad.getKey()       #obtain the state of keys
            if(key != keypad.NULL):
                if(int(key) == 2):
                    self.end_key = int(2)
                    writeState(22)
                    return True

ROWS = 4
COLS = 4
keys =  [   '1','2','3','A',
            '4','5','6','B',
            '7','8','9','C',
            '*','0','#','D'     ]
rowsPins = [12,16,18,36]
colsPins = [35,15,13,11]

def loop():
    print("Started...")
    try:
        keypad = Keypad.Keypad(keys,rowsPins,colsPins,ROWS,COLS)    #creat Keypad object
        keypad.setDebounceTime(50)      #set the debounce time
        writeState(1)

        while(True):
            key = keypad.getKey()       #obtain the state of keys
            if(key != keypad.NULL):     #if there is key pressed, print its key code.
                print ("You Pressed Key : %c "%(key))
                if(int(key) == 1):
                    writeState(11)
                    tag = hashlib.sha1(getTag()[5:9].encode()).hexdigest()
                    try:
                        if(getStatus(tag)[3:-4] == "Client"):
                            if(setPassage(tag) != True):
                                writeState(4)
                        else:
                            writeState(3)
                    except:
                        pass
                elif(int(key) == 4):
                    writeState(10)
                elif(int(key) == 2):
                    writeState(13)
                    tag = hashlib.sha1(getTag()[5:9].encode()).hexdigest()
                    if(getStatus(tag)[3:-4] == "Client"):
                        panier = []
                        detect_end = detectEnd()
                        detect_end.start()
                        keystop=0
                        writeState(14)
                        while(keystop != 2):
                            valid_article = setBoutique(tag)
                            if((valid_article != False) and (detect_end.end_key == 0)):
                                if(articleExist(valid_article,tag)):
                                    panier += [valid_article,]
                                    writeState(15)
                                else:
                                    writeState(16)
                                    time.sleep(2)
                            else:
                                if(detect_end.end_key==2):
                                    panier += [valid_article,]
                                    if(panier[-1] == "9999"):
                                        detect_end.join()
                                        keystop = 2
                                        traitementPanier(panier,tag)
                                    else:
                                        detect_end.join()
                                        keystop = 2
                                        writeState(17)
                    else:
                        writeState(3)
    except KeyboardInterrupt:
        GPIO.cleanup()