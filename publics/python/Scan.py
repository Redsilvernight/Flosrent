import RPi.GPIO as GPIO
import MFRC522
import signal
import sys
from lcdTest import writeState

def end_read(signal,frame):
    GPIO.cleanup()

def getTag():
    signal.signal(signal.SIGINT, end_read)
    MIFAREReader = MFRC522.MFRC522()
    MIFAREReader = detectTag(MIFAREReader)
    tag = str(MIFAREReader.MFRC522_Readstr(8))
    MIFAREReader.MFRC522_StopCrypto1()
    print(tag)
    return tag
	
def detectTag(MIFAREReader):
    continue_reading = True
    while continue_reading:
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
		
        if status == MIFAREReader.MI_OK:
            (status,uid) = MIFAREReader.MFRC522_Anticoll()

        if status == MIFAREReader.MI_OK:
            key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
            MIFAREReader.MFRC522_SelectTag(uid)
            status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

            if status == MIFAREReader.MI_OK:
                continue_reading = False
                return MIFAREReader

def setTag(tag,status = ""):
    if status == "client":
        tag = "flo00"+tag
    elif status == "admin":
        tag = "flo10"+tag
    else:
        tag = str(tag)

    data = []

    for caractere in tag:
        data += [ord(caractere)]

    while len(data) < 16:
        data += [0]

    signal.signal(signal.SIGINT, end_read)
    MIFAREReader = MFRC522.MFRC522()
    MIFAREReader = detectTag(MIFAREReader)
	
    MIFAREReader.MFRC522_Write(8, data)
    MIFAREReader.MFRC522_StopCrypto1()
    return ''


def setArticle(tag):
    data = []
    writeState(1)

    for caractere in tag:
        data += [ord(caractere)]
    signal.signal(signal.SIGINT, end_read)
    MIFAREReader = MFRC522.MFRC522()
    MIFAREReader = detectTag(MIFAREReader)
	
    MIFAREReader.MFRC522_Write(8, data)
    MIFAREReader.MFRC522_StopCrypto1()
    return ''

try:
    if sys.argv[1] == "getTag":
        getTag()
    elif sys.argv[1] == "setTag":
        setTag(sys.argv[2],sys.argv[3])
    elif sys.argv[1] == "setArticle":
        setArticle(sys.argv[2])
except:
    pass