from Scan import *
import time
from model_boutique import *
from lcdTest import writeState

def setBoutique(tag):
    time.sleep(3)
    article = getTag()[5:9]
    if(article != "9999"):
        try:
            if(getInfosArticle(article,tag)[0][6] != 0):
                return article
            else:
                writeState(20)
                return False
        except:
            pass
    else:
        return '9999'