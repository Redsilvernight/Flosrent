from reportlab.pdfgen import canvas
from reportlab.lib import pdfencrypt
from reportlab.lib.units import mm
from reportlab.lib import colors
from datetime import datetime, timedelta
import os

def genFacturePassage(tag,debut,fin,points):
    bye_text = "Au revoir "+tag
    c = canvas.Canvas('gen_facture_passage.pdf')
    c.translate(mm,mm)
    c.setPageSize((80,65))
    c.setFont("Helvetica",3)

    c.drawImage('ticket.jpg',0,54,width=50,height=10)
    c.drawString(((50-len(bye_text))//2),54,bye_text)
    c.drawString(7,51,"Gagner 20% des points de")
    c.drawString(9,48,"votre passage en plus, en")
    c.drawString(4,45,"reservant sur Flosrent.hopto.org")
    c.drawString(4,39,"Entree: ")
    c.drawString(20,39,debut)
    c.drawString(4,34,"Sortie: ")
    c.drawString(20,34,fin)
    c.drawString(4,29,"Points gagnes: ")
    c.drawString(30,29,str(points))
    c.drawString(((50-len('A la prochaine !')) // 2),20,"A la prochaine !")
    c.save()
    os.system('lp -o fit-to-page gen_facture_passage.pdf')

def genFactureBoutique(liste_article,tag):
    merci_text = "Merci "+tag
    now = datetime.now()
    size = len(liste_article) * 5 + 26
    c = canvas.Canvas('gen_facture_boutique.pdf')
    c.translate(mm,mm)
    c.setPageSize((80,size + 6))
    c.setFont("Helvetica",3)

    c.drawImage('ticket.jpg',0,size - 5,width=50,height=10)
    c.drawString(((50-len(merci_text))//2),size - 5,merci_text)
    c.drawString(2,size - 8,"Vos achats du "+now.strftime('%Y-%m-%d %H:%M:%S'))
    ligne = size - 15

    for article in liste_article:
        c.drawString(2,ligne,'- '+str(article[0]))
        c.drawString(40,ligne,str(article[1]) + " Fp")
        ligne -= 5

    c.drawString(((50-len('A la prochaine !')) // 2),ligne-5,"A la prochaine !")

    c.save()
    os.system('lp -o fit-to-page gen_facture_boutique.pdf')

