import I2C_LCD_driver
import time
import sys
import os
from datetime import datetime

global run
run = True

def getPseudo():
    fichier = open("lcdPseudo.txt", "r")
    pseudo = fichier.read()
    fichier.close()
    return str(pseudo)

def setWelcome():
    global valide_state
    if (valide_state == 0):
        try:
            mylcd = I2C_LCD_driver.lcd()
            mylcd.lcd_display_string("Press 1 to Scan", 1, 0)
            mylcd.lcd_display_string(datetime.now().strftime("%d/%m") + "    " + datetime.now().strftime("%H:%M"), 2, 1)
            return ""
        except:
            pass
    else:
        return ''

def setHello():
    global valide_state
    pseudo = getPseudo()[3:-4]
    place_pseudo = ((16 - len(pseudo)) // 2)
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Welcome", 1, 4)
        mylcd.lcd_display_string(str(pseudo), 2, place_pseudo)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(1)

def setUnknowTag():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Badge client", 1, 2)
        mylcd.lcd_display_string("inconnu.", 2, 4)
    except:
        pass
    time.sleep(5)
    valide_state = 0
    writeState(1)

def setLogin():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Scanner pour", 1, 2)
        mylcd.lcd_display_string("vous identifiez.", 2, 0)
    except:
        pass
    time.sleep(2)
    valide_state = 0

def setTrueLogin():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Bienvenue", 1, 3)
        mylcd.lcd_display_string("sur le site.", 2, 2)
    except:
        pass
    time.sleep(5)
    valide_state = 0
    writeState(1)

def setTagLcd():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Scanner pour", 1, 2)
        mylcd.lcd_display_string("ecrire le badge.", 2, 0)
    except:
        pass
    time.sleep(5)
    valide_state = 0


def setBye():
    global valide_state
    pseudo = getPseudo()
    place_pseudo = ((16 - len(pseudo)) // 2)
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Bye", 1, 6)
        mylcd.lcd_display_string(str(pseudo)[3:-4], 2, place_pseudo)
    except:
        pass
    time.sleep(5)
    valide_state = 0
    writeState(1)

def setConfirmWrite():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Le badge", 1, 4)
        mylcd.lcd_display_string("est ecrit.", 2, 3)
    except:
        pass
    time.sleep(5)
    valide_state = 0
    writeState(1)

def setDate():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Nous sommes le ", 1, 1)
        mylcd.lcd_display_string(datetime.now().strftime("%d/%m") + " a " + datetime.now().strftime("%H:%M"), 2, 2)
    except:
        pass
    time.sleep(5)
    valide_state = 0
    writeState(1)

def setErreur():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Une erreur c'est", 1, 0)
        mylcd.lcd_display_string("produite.", 2, 4)
        time.sleep(5)
        valide_state = 0
        writeState(1)
    except:
        valide_state = 0
        writeState(1)

def writeState(lcdstate):
    fichier = open("../../../var/www/html/Flosrent/publics/python/lcdState.txt","w+")
    state = fichier.write(str(lcdstate))
    fichier.close()

def setWaitScan():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Scan ", 1, 4)
        mylcd.lcd_display_string("en cours...", 2, 3)
    except:
        pass
    time.sleep(2)
    valide_state = 0

def setAddArticle():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Boutique ", 1, 4)
        mylcd.lcd_display_string("Scanner badge...", 2, 0)
    except:
        pass
    time.sleep(2)
    valide_state = 0

def setScanArticle():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Scanner vos", 1, 2)
        mylcd.lcd_display_string("articles...", 2, 2)
    except:
        pass
    time.sleep(2)
    valide_state = 0

def setAddconfirm():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Article ajoute", 1, 1)
        mylcd.lcd_display_string("Press 3 to buy", 2, 1)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(14)

def setNoStock():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Echec", 1, 5)
        mylcd.lcd_display_string("Plus en stock", 2, 2)
    except:
        pass
    time.sleep(2)
    valide_state = 0
    writeState(14)


def setUnknowArticle():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Erreur", 1, 5)
        mylcd.lcd_display_string("Article inconnu", 2, 1)
    except:
        pass
    time.sleep(2)
    valide_state = 0
    writeState(14)

def setErreurAchat():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Achat annule", 1, 2)
        mylcd.lcd_display_string("Non confirme", 2, 2)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(1)

def setConfirmAchat():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Achat Reussi", 1, 2)
        mylcd.lcd_display_string("A la prochaine !", 2, 0)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(1)

def setLowPoint():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Achat annule", 1, 2)
        mylcd.lcd_display_string("Points manquants", 2, 0)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(1)

def setPanierVide():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Panier vide", 1, 3)
        mylcd.lcd_display_string("A la prochaine!", 2, 1)
    except:
        pass
    time.sleep(3)
    valide_state = 0
    writeState(1)

def setBadgeConfirm():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Scan confirmation", 1, 0)
        mylcd.lcd_display_string("Pour acheter", 2, 2)
    except:
        pass
    time.sleep(3)
    valide_state = 0




def setConfirmRdv():
    global valide_state
    try:
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Rendez-vous", 1, 3)
        mylcd.lcd_display_string("confirme.", 2, 4)
    except:
        pass
    time.sleep(3)
    writeState(2)


def readState():
    fichier = open("../../../var/www/html/Flosrent/publics/python/lcdState.txt","r")
    state = fichier.read()
    fichier.close()
    return True

def getCWD():
    print(os.getcwd())

def run():
    global run,valide_state
    print('Started...')
    valide_state = 0
    while(run):
        fichier = open("../../../var/www/html/Flosrent/publics/python/lcdState.txt","r")
        try:
            state = fichier.read()
        except:
            pass
        fichier.close()

        if(state == "1"):
            setWelcome()
            #Evite la mise a jour de l'affichage d'accueil pour éviter le plantage dû aux câbles.
            valide_state = 1
        if(state == "2"):
            setHello()
        if(state == "3"):
            setUnknowTag()
        if(state == "4"):
            setErreur()
        if(state == "5"):
            setBye()
        if(state == "6"):
            setLogin()
        if(state == "7"):
            setTrueLogin()
        if(state == "8"):
            setTagLcd()
        if(state == "9"):
            setConfirmWrite()
        if(state == "10"):
            setDate()
        if(state == "11"):
            setWaitScan()
        if(state == "12"):
            setConfirmRdv()
        if(state == "13"):
            setAddArticle()
        if(state == "14"):
            setScanArticle()
        if(state == "15"):
            setAddconfirm()
        if(state == "16"):
            setUnknowArticle()
        if(state == "17"):
            setErreurAchat()
        if(state == "18"):
            setConfirmAchat()
        if(state == "19"):
            setLowPoint()
        if(state == "20"):
            setNoStock()
        if(state == "21"):
            setPanierVide()
        if(state == "22"):
            setBadgeConfirm()