from lcdTest import writeState
from model_passage import getAdmin,getTagWithId
from datetime import datetime, timedelta
import mysql.connector
from facture import *

def articleExist(article,tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_boutique")
    bdd = connection.cursor()

    admin = str(getAdmin(tag))
    table = "boutique_" + getTagWithId(admin)[3:-4]
    article_exist = "SELECT * FROM "+table+" WHERE Badge=%s"
    bdd.execute(article_exist,(article,))
    if(bdd.fetchall()):
        return True
    else:
        return False

def traitementPanier(panier,tag):
    prix_total = float(0.00)
    for article in panier:
        if(article != "9999"):
            produit = getInfosArticle(article,tag)
            prix_total += float(getInfosArticle(article,tag)[0][3])
        else:
            pass
    if(float(getPoints(tag)[0][0]) >= float(prix_total)):
        writeState(18)
        article_facture = []
        for element in panier:
            if(element != "9999"):
                article = getInfosArticle(element,tag)
                setAchat(article[0][0],tag)
                setPoints(article[0][3],tag)
                article_facture += [(article[0][1],article[0][3]),]
                if(article[0][6] != 99):
                    setStock(article[0][0],article[0][6],tag)
        genFactureBoutique(article_facture,getPseudo(tag)[3:-4])
    else:
        writeState(19)
        return False


def getInfosArticle(article,tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_boutique")
    bdd = connection.cursor()
    admin = str(getAdmin(tag))
    table = "boutique_" + getTagWithId(admin)[3:-4]

    get_article = "SELECT * FROM "+table+" WHERE Badge=%s"
    bdd.execute(get_article,(article,))
    return bdd.fetchall()

def getPseudo(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()
    get_admin = "SELECT Pseudo FROM membres WHERE Badge=%s"
    bdd.execute(get_admin,(tag,))
    return str(bdd.fetchall())

def setAchat(article,tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_achats")
    bdd = connection.cursor()
    admin = str(getAdmin(tag))
    table = "achats_" + getTagWithId(admin)[3:-4]
    now = datetime.now()

    set_achat = "INSERT INTO "+table+" (Badge,Produit,Date_achat) VALUES (%s,%s,%s)"
    bdd.execute(set_achat,(tag,article,now.strftime('%Y-%m-%d %H:%M:%S')))
    connection.commit()

    return True

def setPoints(prix,tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()
    admin = str(getAdmin(tag))
    table = "clients_" + getTagWithId(admin)[3:-4]
    points = float(getPoints(tag)[0][0]) - float(prix)

    set_points = "UPDATE "+table+" SET Points = %s WHERE Badge = %s"

    bdd.execute(set_points,(points,tag))
    connection.commit()

    return True

def setStock(article,stock,tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_boutique")
    bdd = connection.cursor()
    admin = str(getAdmin(tag))
    table = "boutique_" + getTagWithId(admin)[3:-4]

    set_stock = "UPDATE "+table+" SET Stock = %s WHERE ID = %s"

    bdd.execute(set_stock,((int(stock) - 1),article))
    connection.commit()

    return True



def getPoints(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()
    admin = str(getAdmin(tag))
    table = "clients_" + getTagWithId(admin)[3:-4]

    get_points = "SELECT Points FROM "+table+" WHERE Badge=%s"
    bdd.execute(get_points,(tag,))
    return bdd.fetchall()