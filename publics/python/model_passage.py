import mysql.connector
from datetime import datetime, timedelta
import time
from lcdTest import writeState
from random import *
from facture import *

def setPassage(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()

    if(tagExist(tag)):
        admin = str(getAdmin(tag))
        pseudo = getPseudo(tag)
        fichier = open("lcdPseudo.txt", "w")
        fichier.write(pseudo)
        fichier.close()
        if(str(getPassage(tag,admin)[0][0]) == "None"):
            if(setRdv(tag,admin) == True):
                writeState(12)
            else:
                writeState(2)
            setEnter(tag,admin)
            return True
        else:
            writeState(5)
            setSortie(tag,admin)
            return True

def getStatus(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()
    get_status = "SELECT Status FROM membres WHERE Badge=%s"
    bdd.execute(get_status,(tag,))
    return str(bdd.fetchall())


def tagExist(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()
    tag_exist = "SELECT * FROM membres WHERE Badge=%s"
    bdd.execute(tag_exist,(tag,))
    if(bdd.fetchall()):
        return True
    else:
        return False

def getPseudo(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()
    get_admin = "SELECT Pseudo FROM membres WHERE Badge=%s"
    bdd.execute(get_admin,(tag,))
    return str(bdd.fetchall())

def getAdmin(tag):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()
    get_admin = "SELECT Id_admin FROM membres WHERE Badge=%s"
    bdd.execute(get_admin,(tag,))
    return str(bdd.fetchall())[2:-3]

def getPassage(tag,admin):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()

    table_name = "clients_" + getTagWithId(admin)[3:-4]
    get_passage = "SELECT Id_passage FROM "+table_name+" WHERE Badge=%s"
    bdd.execute(get_passage,(tag,))
    return bdd.fetchall()

def getTagWithId(id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_membres")
    bdd = connection.cursor()

    get_tag = "SELECT Badge FROM membres WHERE ID=%s"
    bdd.execute(get_tag,(id,))
    return str(bdd.fetchall())

def setEnter(tag,admin):
    id = randomId(admin)
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()

    table = "passages_" + getTagWithId(admin)[3:-4]
    now = datetime.now()

    insert_passage = "INSERT INTO "+table+" (Badge,Date_entree,Points,Id_passage,Date_sortie) VALUES (%s,%s,%s,%s,NULL)"
    bdd.execute(insert_passage,(tag,now.strftime('%Y-%m-%d %H:%M:%S'),0,id))
    connection.commit()

    setIdEnter(tag,admin,id)
    return True

def randomId(admin):
    id = str(randint(0,9999))
    table_name = "passages_" + getTagWithId(admin)[3:-4]
    if(len(id) < 4):
        while (len(id) < 4):
            id = str(id) + "0"
    while(idExist(id,table_name) != "[]" ):
        id += 1
    return id

def idExist(id,table):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()

    id_exist = "SELECT * FROM "+table+" WHERE Id_passage=%s"
    bdd.execute(id_exist,(id,))
    return str(bdd.fetchall())

def setIdEnter(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()
    table = "clients_" + getTagWithId(admin)[3:-4]

    set_id_enter = "UPDATE "+table+" SET Id_passage = %s WHERE Badge = %s"
    bdd.execute(set_id_enter,(id,tag))
    connection.commit()
    return True

def getIdEnter(tag,admin):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()
    table = "clients_" + getTagWithId(admin)[3:-4]

    get_id_enter = "SELECT Id_passage FROM "+table+" WHERE Badge=%s"
    bdd.execute(get_id_enter,(tag,))
    return str(bdd.fetchall())[2:6]


def setSortie(tag,admin):
    id = getIdEnter(tag,admin)
    setHeureSortie(tag,admin,id)

    table = "clients_" + getTagWithId(admin)[3:-4]
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()

    set_id_null = "UPDATE "+table+" SET Id_passage = NULL WHERE Badge = %s"
    bdd.execute(set_id_null,(tag,))
    connection.commit()

    calculPoint(tag,admin,id)



def setHeureSortie(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()
    table = "passages_" + getTagWithId(admin)[3:-4]
    now = datetime.now()

    set_heure_sortie = "UPDATE "+table+" SET Date_sortie = %s WHERE Id_passage = %s"
    bdd.execute(set_heure_sortie,(now.strftime('%Y-%m-%d %H:%M:%S'),id))
    connection.commit()

    return ''

def calculPoint(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()
    table = "passages_" + getTagWithId(admin)[3:-4]

    entree = getEntree(tag,admin,id)
    sortie = getSortie(tag,admin,id)

    duration_seconde = (sortie - entree).total_seconds()
    points = round((((duration_seconde*5)/100)/60),2)
    setPointsPassage(points,tag,admin,id)
    genFacturePassage(getPseudo(tag)[3:-4],str(entree),str(sortie),points)

def getEntree(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()
    table = "passages_" + getTagWithId(admin)[3:-4]

    get_date_entree = "SELECT Date_entree FROM "+table+" WHERE Id_passage=%s"
    bdd.execute(get_date_entree,(id,))

    return forgeDate(str(bdd.fetchall())[20:-4])

def getSortie(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()
    table = "passages_" + getTagWithId(admin)[3:-4]

    get_date_sortie = "SELECT Date_sortie FROM "+table+" WHERE Id_passage=%s"
    bdd.execute(get_date_sortie,(id,))

    return forgeDate(str(bdd.fetchall())[20:-4])

def forgeDate(date):
    format_date = '%Y-%m-%d %H:%M:%S'
    date = date.replace(',', '-',1)
    date = date.replace(',', '-',2)
    date = date.replace(',', ':',4)
    date = date.replace(' ', '')
    date = date.replace('-', ' ',3)
    date = date.replace(' ', '-',2)

    return datetime.strptime(date, format_date)

def setPointsPassage(points,tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_passages")
    bdd = connection.cursor()
    table = "passages_" + getTagWithId(admin)[3:-4]

    if(len(getConfirmRdv(tag,admin)) > 0):
        pourcentage = points * 20 // 100
        points += pourcentage
        setConfirmRdv(tag,admin,getConfirmRdv(tag,admin)[0][0])

    set_points = "UPDATE "+table+" SET Points = %s WHERE Id_passage = %s"

    bdd.execute(set_points,(points,id))
    connection.commit()
    setPointsClient(points,tag,admin)

def setConfirmRdv(tag,admin,id):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_rdv")
    bdd = connection.cursor()
    table = "rdv_" + getTagWithId(admin)[3:-4]

    set_points_rdv = "UPDATE "+table+" SET Etat = %s WHERE id = %s"
    bdd.execute(set_points_rdv,(2,int(id)))
    connection.commit()

    return True

def setPointsClient(points,tag,admin):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_clients")
    bdd = connection.cursor()
    table = "clients_" + getTagWithId(admin)[3:-4]

    get_points = "SELECT Points FROM "+table+" WHERE Badge = %s"
    bdd.execute(get_points,(tag,))
    points_client = str(bdd.fetchall())[2:-3]

    points_total = float(points_client) + points
    set_points = "UPDATE "+table+" SET POINTS = %s WHERE Badge = %s"
    bdd.execute(set_points,(points_total,tag))

    connection.commit()

    return True

def getRdv(tag,admin):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_rdv")
    bdd = connection.cursor()
    table = "rdv_" + getTagWithId(admin)[3:-4]

    get_rdv = "SELECT id,Etat FROM "+table+" WHERE Badge = %s AND Date_rdv BETWEEN %s AND %s AND Etat IS NULL"
    bdd.execute(get_rdv,(tag,datetime.now() + timedelta(hours = 0, minutes = -10),datetime.now() + timedelta(hours = 0, minutes = 10),))

    return bdd.fetchall()

def setRdv(tag,admin):
    if(len(getRdv(tag,admin)) > 0):
        if getRdv(tag,admin)[0][1] != 1:
            connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_rdv")
            bdd = connection.cursor()
            table = "rdv_" + getTagWithId(admin)[3:-4]

            set_points_rdv = "UPDATE "+table+" SET Etat = %s WHERE id = %s"
            bdd.execute(set_points_rdv,(1,int(getRdv(tag,admin)[0][0])))
            connection.commit()

            return True
        else:
            return False
    else:
        return False

def getConfirmRdv(tag,admin):
    connection = mysql.connector.connect(host="localhost",user="root",password="roule1grosboze",database="flosrent_rdv")
    bdd = connection.cursor()
    table = "rdv_" + getTagWithId(admin)[3:-4]

    get_rdv = "SELECT id,Etat FROM "+table+" WHERE Badge = %s AND Etat = %s"
    bdd.execute(get_rdv,(tag,1))

    return bdd.fetchall()