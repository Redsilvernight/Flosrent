import RPi.GPIO as GPIO
import sys
from mfrc522 import SimpleMFRC522
from Scan import setTag,getTag

def loop(text):
    reader = SimpleMFRC522()
    run = True
    while run:
        print("Now place your tag to write")
        try:
            try:
                reader.write(text)
            except:
                try:
                    setTag(text)
                except:
                    print("Une erreur s'est produite lors de l'écriture.")
        except:
            print("Une erreur s'est produite lors de l'écriture")


        badge = getTag()

        if(badge[:9] == text):
            print("Ecriture terminée avec succès")
            run = False
            GPIO.cleanup()
            return True
        else:
            print("Une erreur s'est produite avec la confirmation")

try:
    if sys.argv[1] == "setArticle":
        loop(sys.argv[2])
except:
    pass
