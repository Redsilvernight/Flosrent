<?php
session_start();

if(isset($_GET['page']))
{
    $page = $_GET['page'];

    if(isset($_GET['tag']))
    {
        $tag = $_GET['tag'];
    }
    elseif(isset($_SESSION['tag']))
    {
        $tag = $_SESSION['tag'];
    }
    else
    {
        session_destroy();
        $tag = "";
    }

    switch($page)
    {
        case 1:
            header('Location: controlers/connexion.php?scan=True');
        break;
        case 2:
            header('Location: vues/identifiant.php');
        break;
        case 3:
            header('Location: vues/admin/compte_admin.php?tag='.$tag);
        break;
        case 4:
            header('Location: vues/admin/clients_admin.php?tag='.$tag);
        break;
        case 5:
            header('Location: vues/admin/boutique_admin.php?tag='.$tag);
        break;
        case 6:
            header('Location: vues/admin/stats_admin.php');
        break;
        case 7:
            header('Location: vues/admin/nouveau_client.php?tag='.$tag);
        break;
        case 9:
            header('Location: vues/admin/add_article.php?tag='.$tag);
        break;
        case 10:
            header('Location: vues/user/reservation_user.php?tag='.$tag);
        break;
        case 11:
            header('Location: vues/restaur_mdp.php?tag='.$tag);
        break;
        case 12:
            header('Location: vues/user/Compte_user.php?tag='.$tag);
        break;
        case 13:
            header('Location: vues/user/boutique_user.php?tag='.$tag);
        break;
        case 14:
            header('Location: vues/deconnexion.php');
        break;
        case 15:
            header('Location: vues/Identifiant_scan.php');
        break;
        case 16:
            header('Location: vues/admin/info_client.php?id='.$_GET['id'].'&tag='.$tag);
        break;
        case 17:
            header('Location: vues/admin/add_badge_article.php?tag='.$tag.'&product_name='.$_GET['product_name']);
        break;
        case 18:
            header('Location: controlers/user/buy_article.php?tag='.$tag.'&article='.$_GET['article']);
        break;
        case 19:
            header('Location: vues/admin/infos_produit.php?tag='.$tag.'&product='.$_GET['product']);
        break;
    }
}
else
{
    header('Location: vues/Connexion.php');
}
