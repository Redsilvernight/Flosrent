"""Fichier d'installation de notre script salut.py."""

from cx_Freeze import setup, Executable

# On appelle la fonction setup
setup(
    name = "auto-pull",
    version = "0.1",
    description = "Auto-pull",
    executables = [Executable("Auto_pull.pyw", base = "Win32GUI")],
)