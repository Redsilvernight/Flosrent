<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/Connexion.css" rel="stylesheet">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
        <link href="../publics/css/Videos.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Accueil</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>
            
            <section>
                <div id="container_bg">
                    <video autoplay muted loop id="movie_bg">
                        <source src="../publics/Videos/CCTV.mp4" type="video/mp4">
                    </video>
                    <div id="div_login" class="flosrent_bg">
                        <h1>S'identifier</h1>
                        <a href="../routeur.php?page=1"><p class="flosrent_btn" id="p_scan">Scan</p></a>
                        <a href="../routeur.php?page=2"><p class="flosrent_btn" id="p_login">S'identifier</p></a>
                    </div>
                </div>
            </section>
            <?php include("footer.php"); ?>
        </div>
    </body>
</html>
