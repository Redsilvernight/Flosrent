<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
        <link href="../publics/css/Scan.css" rel="stylesheet">
        <link href="../publics/css/Videos.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Scan</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Banniere_logo" id="img_banniere"></a>
            </header>

            <section>
                <div id="container_bg">
                    <video autoplay muted loop id="movie_bg">
                        <source src="../publics/Videos/CCTV.mp4" type="video/mp4">
                    </video>
                    <div id="div_form" class="flosrent_bg">
                        <h1>Scan</h1>
                            <img src="../publics/Images/Scan.png" alt="Logo_scan" id="img_scan"></a>
                        <p>. . .</p>
                    </div>
                </div>
            </section>
            <?= include("footer.php"); ?>
        </div>
    </body>
</html>

