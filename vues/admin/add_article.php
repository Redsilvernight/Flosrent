<?php
session_start();
require('../../controlers/verif_connection.php');

if(testConnection($_GET['tag']) == TRUE)
{


?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <link href="../../publics/css/admin/add_article.css" rel="stylesheet">
        <title>Flo's Rent-Add article</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <section>
                <div class="flosrent_div">
                    <form method="POST" action="../../controlers/admin/controler_admin.php" enctype="multipart/form-data" id="form_add_article">
                    <h1 class="flosrent.txt">Nouveau produit</h1>
                    <div id="div_name">
                        <label for="product_name" class="flosrent_label">Nom du produit : </label>
                        <input type="text" name = "product_name" class="flosrent_input" placeholder="Ex: Bouteille d'eau">
                    </div>
                    <div id="div_description">
                        <label for="product_description" class="flosrent_label" id="label_description">Desription du produit : </label>
                        <textarea id="product_description" name = "product_description" class="flosrent_input" rows="6" cols="30" placeholder="Ex: Une bouteille d'eau de la marque Cristaline."></textarea>
                    </div>
                    <div id="div_prix">
                        <label for="product_price" class="flosrent_label">Prix du produit : </label>
                        <input type="text" name="product_price" class="flosrent_input" placeholder="Ex: 2">
                        <label for="product_price" class="flosrent_label">Points</label>
                    </div>
                    <div id="div_photo">
                        <label for="product_photo" class="flosrent_label">Photo du produit : </label>
                        <input type="file" name="product_photo" class="flosrent_input" placeholder="Parcourir">
                    </div>
                    <div id="div_stock">
                        <label for="product_stock" class="flosrent_label">Nombre en stock : </label>
                        <input type="text" name="product_stock" class="flosrent_input" placeholder="Ex: 14">
                    </div>
                    <input type="submit" class="flosrent_btn" name="btn_add_product"value="Ajouter" id="btn_add">
                    
                
                    </form>
                </div>
            
            
            </section>
        </div>
    </body>
</html>
<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}
