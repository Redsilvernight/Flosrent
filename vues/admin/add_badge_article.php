<?php

require('../../controlers/admin/controler_admin.php');

$tag = randomTagArticle();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
		<link href="../../publics/css/flosrent.css" rel="stylesheet">
		<link href="../../publics/css/admin/ajout_badge.css" rel="stylesheet">
        <title>Flo's Rent-Add article</title>
    </head>

    <body>
	<div id="div_main">
	        <header>
	            <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Banniere_logo" id="img_banniere"></a>
	        </header>
	
	        <section>
	            <div class="flosrent_div">
	            	<form id="form_add_tag_article" name="form_add_tag" method="post" action='../../controlers/admin/controler_admin.php?product_name=<?php echo $_GET['product_name'] ?>&tag=<?php echo $tag ?>'>
	                	<h1>Numéro du produit</h1>
	              		<label for="tag_number" class="flosrent_label">Numero<br></label>
	              		<input type="text" name="tag_number" class="flosrent_input" id="inp_id" value="<?php echo $tag ?>" disabled="disabled">
	              		<br>
	              		<input type="submit" class="flosrent_btn" id="btn_scan" value="Scan" name="btn_add_tag_article">
	            	</form>
	          </div>
	        </section>
	
        	<?php include("../footer.php"); ?>
        </div>
  </body>
</html>
