<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/admin/model_admin.php');

if(testConnection($_GET['tag']) == TRUE)
{
    $get_article = getArticle($_SESSION['tag']);
    

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/boutique_admin.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Boutique</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>
            
            <?php include("nav_admin.php"); ?>
            
            <section>
                <div id="div_nouveau_article" class="flosrent_div">
                    <a href="../../routeur.php?page=9&tag=<?php echo $_SESSION['tag'];?>"><h1 id="p_new_article" class="flosrent_btn">Nouvel article</h1></a>
                </div>
                
                <div id="div_boutique" class="flosrent_div">
                    <h1>Articles</h1>
                    <table class='flosrent_table' id="table_article">
                        <tr>
                            <th class="flosrent_th">Nom</th>
                            <th class="flosrent_th">Description</th>
                            <th class="flosrent_th">Prix</th>
                            <th class="flosrent_th">Stock</th>
                            <th class="flosrent_th">Badge</th>
                        </tr>

                    <?php   
                            $switch = 0;
                            while($article = $get_article->fetch())
                            { 
                                if($switch == 0)
                                {?>
                                    <tr class="flosrent_tr">
                                        <td class="flosrent_td_white"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Nom'];?></p></a></td>
                                        <td class="flosrent_td_white"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Description'];?></p></a></td>
                                        <td class="flosrent_td_white"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Prix'];?></p></a></td>
                                        <td class="flosrent_td_white"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Stock'];?></p></a></td>
                                        <td class="flosrent_td_white"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Badge'];?></p></a></td>
                                    </tr>
                                    <?php   $switch = 1;
                                } 
                                elseif($switch == 1)
                                {?>
                                    <tr class="flosrent_tr">
                                        <td class="flosrent_td_purple"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Nom'];?></p></a></td>
                                        <td class="flosrent_td_purple"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Description'];?></p></a></td>
                                        <td class="flosrent_td_purple"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Prix'];?></p></a></td>
                                        <td class="flosrent_td_purple"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Stock'];?></p></a></td>
                                        <td class="flosrent_td_purple"><a href="../../routeur.php?page=19&product=<?php echo $article['ID'];?>"><p><?php echo $article['Badge'];?></p></a></td>
                                    </tr>
                                    <?php $switch = 0;
                                }
                            }
                            $get_article -> closeCursor();?>
                    </table>
                </div>
            </section>
            
            <?php include("../footer.php"); ?>
        </div>
    </body>
</html>
<?php
}