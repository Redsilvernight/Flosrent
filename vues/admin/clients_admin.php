<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/admin/model_admin.php');

if(testConnection($_GET['tag']) == TRUE)
{
    $client_to_client = getClientToClient($_SESSION['tag']);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/clients_admin.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Compte</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <?php include("nav_admin.php"); ?>

            <section>
                <div class="flosrent_div" id="div_add">
                    <div id="div_add_btn">
                        <img src="../../publics/Images/add.png" alt="img_add_client" id="img_add">
                        <a href="../../routeur.php?page=7&tag=<?php echo $_SESSION['tag']; ?>" ><p class="flosrent_btn">Nouveau client</p></a>
                    </div>
                </div>
                <div id="div_clients" class="flosrent_div">
                    <p class="flosrent_txt" id="p_clients">Clients</p>
                    <table class='flosrent_table' id="table_clients">
                        <tr>
                            <th class="flosrent_th">Pseudo</th>
                            <th class="flosrent_th">Prenom</th>
                            <th class="flosrent_th">Nom</th>
                            <th class="flosrent_th">Points</th>
                            <th class="flosrent_th">Date inscription</th>
                        </tr>

                    <?php    if($client_to_client == TRUE)
                                {
                                    $switch = 0;
                                    while($donnee_client = $client_to_client->fetch())
                                    { 
                                        $client_to_membre = getClientToMembre($donnee_client['Badge']);

                                        while($donnee_membre = $client_to_membre->fetch())
                                        { 
                                            if($switch == 0)
                                            {?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_white"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Pseudo'];?></p></a></td>
                                                    <td class="flosrent_td_white"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Prenom'];?></p></a></td>
                                                    <td class="flosrent_td_white"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Nom'];?></p></a></td>
                                                    <td class="flosrent_td_white"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_client['Points'];?></p></a></td>
                                                    <td class="flosrent_td_white"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Date_inscription'];?></p></a></td>
                                                </tr>
                                                <?php   $switch = 1;
                                            }
                                            elseif($switch == 1)
                                            {?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_purple"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Pseudo'];?></p></a></td>
                                                    <td class="flosrent_td_purple"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Prenom'];?></p></a></td>
                                                    <td class="flosrent_td_purple"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Nom'];?></p></a></td>
                                                    <td class="flosrent_td_purple"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_client['Points'];?></p></a></td>
                                                    <td class="flosrent_td_purple"><a href="../../routeur.php?page=16&id=<?php echo $donnee_client['ID'];?>&tag=<?php echo $_SESSION['tag']; ?>"><p><?php echo $donnee_membre['Date_inscription'];?></p></a></td>
                                                </tr>
                                                <?php $switch = 0;
                                            }
                                        }
                                        $client_to_membre -> closeCursor();
                                    } 
                                    $client_to_client -> closeCursor();
                                }
                                else
                                {

                                } ?>
                    </table>
                </div>
            </section>

            <?php include("../footer.php"); ?>
        </div>
    </body>
</html>
<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}
