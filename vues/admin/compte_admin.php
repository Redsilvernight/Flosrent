<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/admin/model_admin.php');

if(testConnection($_GET['tag']) == TRUE)
{
    $_SESSION['pseudo'] = getPseudo($_SESSION['tag']);

    $get_passages = getPassages($_SESSION['tag']);

    require 'src/bootstrap.php';

    $pdo = get_pdo();
    $events = new Calendar\Events($pdo);
    $month = new Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null);
    $start = $month->getStartingDay();
    $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
    $weeks = $month->getWeeks();
    $end = $start->modify('+' . (6 + 7 * ($weeks -1)) . ' days');
    $events = $events->getEventsBetweenByDay($start, $end, $_SESSION['tag']);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/compte_admin.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="public/css/calendar.css">
        <title>Flo's Rent-Compte</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>
            
            <?php include("nav_admin.php"); ?>
            
            <section>
                <h1 class="flosrent_txt">Bonjour <?php echo $_SESSION['pseudo'];?></h1>
                <div class="flosrent_div" id="div_passage">
                    <p class="flosrent_txt" id="p_passage">Passages</p>
                        <table class="flosrent_table" id="table_passages">
                            <tr>
                                <th class="flosrent_th">Prenom</th>
                                <th class="flosrent_th">Nom</th>
                                <th class="flosrent_th">Date d'entrée</th>
                                <th class="flosrent_th">Date de sortie</th>
                                <th class="flosrent_th">Points gagnés</th>
                            </tr>
                                <?php    
                                    $switch = 0;
                                    if($get_passages == TRUE)
                                    {
                                        while($passages = $get_passages->fetch())
                                        { 
                                            $get_info_client = getInfosClient($passages['Badge']);
                                            while($infos_client = $get_info_client->fetch())
                                            {
                                                if($switch == 0)
                                                {
                                                    ?>
                                                    <tr class="flosrent_tr">
                                                    <td class="flosrent_td_white"><p><?php echo $infos_client['Prenom'];?></p></td>
                                                    <td class="flosrent_td_white"><p><?php echo $infos_client['Nom'];?></p></td>
                                                    <?php
                                                    if($passages['Date_sortie'] == "1111-11-11 11:11:11")
                                                    {
                                                    ?>
                                                        <td class="flosrent_td_white"><p><?php echo $passages['Date_entree'];?></p></td>
                                                        <td class="flosrent_td_white"><p>Annulé</p></td>
                                                        <td class="flosrent_td_white"><p>Annulé</p></td>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                        <td class="flosrent_td_white"><?php echo $passages['Date_entree'];?></td>
                                                        <td class="flosrent_td_white"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                        <td class="flosrent_td_white"><?php echo $passages['Points'];?></td>
                                                    <?php
                                                    }
                                                    ?>
                                                    </tr>
                                                    <?php   $switch = 1;
                                                }
                                                elseif($switch == 1)
                                                    { 
                                                        ?>
                                                        <tr class="flosrent_tr">
                                                            <td class="flosrent_td_purple"><p><?php echo $infos_client['Prenom'];?></p></td>
                                                            <td class="flosrent_td_purple"><p><?php echo $infos_client['Nom'];?></p></td>
                                                            <?php
                                                            if($passages['Date_sortie'] == "1111-11-11 11:11:11")
                                                            {
                                                            ?>
                                                            <td class="flosrent_td_purple"><p><?php echo $passages['Date_entree'];?></p></p></td>
                                                            <td class="flosrent_td_purple"><p>Annulé</p></td>
                                                            <td class="flosrent_td_purple"><p>Annulé</p></td>
                                                            <?php
                                                            }
                                                            else
                                                            {
                                                            ?>
                                                                <td class="flosrent_td_purple"><?php echo $passages['Date_entree'];?></td>
                                                                <td class="flosrent_td_purple"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                                <td class="flosrent_td_purple"><?php echo $passages['Points'];?></td>
                                                            <?php
                                                            }
                                                            ?>
                                                        </tr>
                                                    <?php   
                                                    $switch = 0;
                                                    }
                                            }
                                            $get_info_client -> closeCursor();
                                        } 
                                        $get_passages-> closeCursor();
                                    }
                                    else
                                    {

                                    } ?>
                                    
                        </table>
                </div>
                
                <div id="div_calendrier" class="flosrent_div">
                    <h1 class="flosrent_txt">Réservations</h1>

                    <div class="calendar">

                        <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
                            <h1><?= $month->toString(); ?></h1>

                            

                            <div>
                            <a href="compte_admin.php?tag=<?= $_SESSION['tag'];?>&month=<?= $month->previousMonth()->month; ?>&year=<?= $month->previousMonth()->year; ?>" class="btn btn-primary">&lt;</a>
                            <a href="compte_admin.php?tag=<?= $_SESSION['tag'];?>&month=<?= $month->nextMonth()->month; ?>&year=<?= $month->nextMonth()->year; ?>" class="btn btn-primary">&gt;</a>
                            </div>
                        </div>

                        <table class="calendar__table calendar__table--<?= $weeks; ?>weeks">
                            <?php for ($i = 0; $i < $weeks; $i++): ?>
                                <tr>
                                    <?php
                                    foreach($month->days as $k => $day):
                                        $date = $start->modify("+" . ($k + $i * 7) . " days");
                                        $eventsForDay = $events[$date->format('Y-m-d')] ?? [];
                                        $isToday = date('Y-m-d') === $date->format('Y-m-d');
                                        ?>
                                    <td class="<?= $month->withinMonth($date) ? '' : 'calendar__othermonth'; ?> <?= $isToday ? 'is-today' : ''; ?>">
                                        <?php if ($i === 0): ?>
                                            <div class="calendar__weekday"><?= $day; ?></div>
                                        <?php endif; ?>
                                        <p class="calendar__day"><?= $date->format('d'); ?> </p>
                                        <?php foreach($eventsForDay as $event): ?>
                                            <div class="calendar__event">
                                                <?php $tag = $event->getTag();
                                                    $get_pseudo = getClientToMembre($tag);
                                                    $pseudo = $get_pseudo->fetch();

                                                    switch($event -> getState())
                                                    {
                                                        case "":
                                                ?>
                                                            <a class = "flosrent_label" href="../confirmation.php?type=3&client=&id=<?php echo $event->getId();?>"><p><?php echo $event->getStart()->format('H:i') ?> - <?= $pseudo['Pseudo'] ?></p></a>            
                                                        <?php
                                                        break;
                                                        case 1:
                                                        ?>
                                                            <p id="rdv_en_cours"><?php echo $event->getStart()->format('H:i') ?> - <?= $pseudo['Pseudo'] ?></p></a>            
                                                        <?php
                                                        break;
                                                        case 2:
                                                        ?>
                                                            <p id="rdv_valide"><?php echo $event->getStart()->format('H:i') ?> - <?= $pseudo['Pseudo'] ?></p></a>            
                                                        <?php
                                                        break;
                                                    }
                                                    
                                                ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endfor; ?>
                        </table>
                        </div>
                </div>
                <a href="../../routeur.php?page=11&tag=<?php echo $_SESSION['tag'];?>"><p class="flosrent_btn" id="p_reset_mdp">Réinitialiser le mot de passe</p></a>
            </section>
            
            <?php include("../footer.php"); ?>
        </div>
        
    </body>
</html>
<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}
