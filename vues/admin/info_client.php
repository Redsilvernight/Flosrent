<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../controlers/admin/controler_infos_client.php');
require('../../models/admin/model_info.php');




if(testConnection($_GET['tag']) == TRUE)
{
    $get_client = getInfosClient($_GET['id'])->fetch();
    $get_membres = getInfosMembres($get_client['Badge'])->fetch();
    

    if(empty($get_client['Id_passage']))
    {
        $etat = "Absent";
    }
    else
    {
        $etat = "Present";
    }
    
    $get_passages = getPassages($_SESSION['tag'],getInfosClient($_GET['id'])->fetch()['Badge']);
    $get_rdv = getRdv($_SESSION['tag'],getInfosClient($_GET['id'])->fetch()['Badge']);
    $get_achats = getAchats($get_client['Badge'],$_SESSION['tag']);
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/info_client.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Infos client</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <?php include("nav_admin.php"); ?>

            <section>
                <div class="flosrent_div">
                    <div id="div_name">
                        <h1 class="flosrent_txt" id="txt_prenom"><?php echo $get_membres['Prenom'];?></h1>
                        <h2 class="flosrent_label" id="txt_pseudo"><?php echo $get_membres['Pseudo'];?></h2>
                    </div>

                    <h1 class="flosrent_txt" id="txt_points"><?php echo $get_client['Points'];?> Points</h1>

                    <div id="div_etat">
                        <h1 class="flosrent_txt" id="txt_etat"><?php echo $etat; ?></h1>
                        <?php 
                        if($etat == "Present")
                        {   ?>
                            <img src="../../publics/Images/present.png" id="img_present" alt="Image présent">
                        <?php
                        }
                        else
                        {
                            ?>
                            <img src="../../publics/Images/absent.png" id="img_absent" alt="Image absent">
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                    if($etat == "Present")
                    {
                    ?>
                        <a href="../confirmation.php?type=2&tag=<?php echo $_SESSION['tag'];?>&client=<?php echo $get_client['Badge']; ?>&id=<?php echo $get_client['Id_passage'];?>"><p id="txt_stop">Stopper le passage</p></a>
                    <?php
                    }
                    ?>
                </div>

                <div class="flosrent_div" id="div_rdv">
                        <h1 class="flosrent_txt">Rendez-vous</h1>
                        <table class="flosrent_table" id="table_passages">
                        <tr>
                            <th class="flosrent_th">Pseudo</th>
                            <th class="flosrent_th">Date Rendez-vous</th>
                            <th class="flosrent_th">Etat</th>
                        </tr>
                        <?php    
                            $switch = 0;
                                while($rdv = $get_rdv->fetch())
                                { 
                                    if($switch == 0)
                                    {
                        ?>
                                        <tr class="flosrent_tr">
                                            <td class="flosrent_td_white"><p><?php echo $get_membres['Pseudo'];?></p></td>
                                            <td class="flosrent_td_white"><p><?php echo $rdv['Date_rdv'];?></p></td>
                                            <td class="flosrent_td_white"><p><?php echo stateRdv($rdv['Etat']);?></p></td>
                                        </tr>
                                        <?php   
                                        $switch = 1;
                                    }
                                    elseif($switch == 1)
                                    { 
                                        ?>
                                        <tr class="flosrent_tr">
                                            <td class="flosrent_td_purple"><p><?php echo $get_membres['Pseudo'];?></p></td>
                                            <td class="flosrent_td_purple"><p><?php echo $rdv['Date_rdv'];?></p></p></td>
                                            <td class="flosrent_td_purple"><p><?php echo stateRdv($rdv['Etat']);?></p></td>
                                        </tr>
                                        <?php   
                                        $switch = 0;
                                    }
                                            
                                } 
                                $get_rdv-> closeCursor(); ?>    
                        </table>
                </div>

                <div class="flosrent_div" id="div_achats">
                    <h1 class="flosrent_txt">Achats</h1>
                    <table class="flosrent_table" id="table_achats">
                            <tr>
                                <th class="flosrent_th">Produits</th>
                                <th class="flosrent_th">Prix</th>
                                <th class="flosrent_th">Date d'achat</th>
                            </tr>
                                <?php    
                                    $switch = 0;
                                    while($achat = $get_achats->fetch())
                                    { 
                                        $get_article = getAchatsArticle($achat['Produit'],$_SESSION['tag']);
                                        while($article = $get_article->fetch())
                                        {
                                            if($switch == 0)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_white"><?php echo $article['Nom'];?></td>
                                                    <td class="flosrent_td_white"><p><?php echo $article['Prix'];?></p></td>
                                                    <td class="flosrent_td_white"><p></p><?php echo $achat['Date_achat'];?></p></td>
                                                </tr>
                                    <?php       $switch = 1;
                                            }
                                            elseif($switch == 1)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_purple"><?php echo $article['Nom'];?></td>
                                                    <td class="flosrent_td_purple"><p><?php echo $article['Prix'];?></p></td>
                                                    <td class="flosrent_td_purple"><p></p><?php echo $achat['Date_achat'];?></p></td>
                                                </tr>
                                    <?php       $switch = 0;
                                            }
                                        }
                                        $get_article -> closeCursor();
                                    }
                                    $get_achats-> closeCursor();
                                ?>    
                    </table>
                </div>

                <div class="flosrent_div" id="div_passages">
                    <h1 class="flosrent_txt">Passages</h1>
                    <table class="flosrent_table" id="table_passages">
                        <tr>
                            <th class="flosrent_th">Pseudo</th>
                            <th class="flosrent_th">Date d'entrée</th>
                            <th class="flosrent_th">Date de sortie</th>
                            <th class="flosrent_th">Points gagnés</th>
                        </tr>
                        <?php    
                            $switch = 0;
                                while($passages = $get_passages->fetch())
                                { 
                                    $get_info_client = getInfosClient($passages['Badge']);
                                    
                                    if($switch == 0)
                                    {
                        ?>
                                        <tr class="flosrent_tr">
                                            <td class="flosrent_td_white"><p><?php echo $get_membres['Pseudo'];?></p></td>
                                            <?php
                                            if($passages['Date_sortie'] == "1111-11-11 11:11:11")
                                            {
                                            ?>
                                                <td class="flosrent_td_white"><p><?php echo $passages['Date_entree'];?></p></td>
                                                <td class="flosrent_td_white"><p>Annulé</p></td>
                                                <td class="flosrent_td_white"><p>Annulé</p></td>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <td class="flosrent_td_white"><?php echo $passages['Date_entree'];?></td>
                                                <td class="flosrent_td_white"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                <td class="flosrent_td_white"><?php echo $passages['Points'];?></td>
                                            <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php   
                                        $switch = 1;
                                    }
                                    elseif($switch == 1)
                                    { 
                                        ?>
                                        <tr class="flosrent_tr">
                                            <td class="flosrent_td_purple"><p><?php echo $get_membres['Pseudo'];?></p></td>
                                        <?php
                                            if($passages['Date_sortie'] == "1111-11-11 11:11:11")
                                            {
                                        ?>
                                                <td class="flosrent_td_purple"><p><?php echo $passages['Date_entree'];?></p></p></td>
                                                <td class="flosrent_td_purple"><p>Annulé</p></td>
                                                <td class="flosrent_td_purple"><p>Annulé</p></td>
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                                <td class="flosrent_td_purple"><?php echo $passages['Date_entree'];?></td>
                                                <td class="flosrent_td_purple"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                <td class="flosrent_td_purple"><?php echo $passages['Points'];?></td>
                                        <?php
                                            }
                                        ?>
                                        </tr>
                                        <?php   
                                        $switch = 0;
                                    }
                                            
                                } 
                                $get_passages-> closeCursor(); ?>    
                        </table>
                </div>

                <a href="../confirmation.php?type=5&tag=<?php echo $_SESSION['tag'];?>&client=<?php echo $get_client['Badge']; ?>&id="><p id="txt_delete">Supprimer le client</p></a>





            </section>

            <?php include("../footer.php"); ?>
        </div>
    </body>
</html>

<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}



    