<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/admin/model_add_product.php');
require('../../models/model_connection.php');

if(testConnection($_GET['tag']) == TRUE)
{
    $article = getInfosArticle($_SESSION['tag'],$_GET['product'])->fetch();
    $get_achats = getAchatsId($_SESSION['tag'],$article['ID']);

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/infos_articles.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Infos produit</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <?php include("nav_admin.php"); ?>

            <section>
                <div id="div_infos">
                    <p id="p_nbr_ventes" class="flosrent_txt">Nombre de ventes: <?php echo $get_achats->rowCount();?></p>
                    <img src="../../publics/Product_photos/<?php echo $article['Lien_photo'];?>" width="400px" height="200px" id="photo_product" alt="photo du produit">
                    <h1 id="h1_name" class="flosrent_txt"><?php echo $article['Nom'];?></h1>
                </div>
            
                <div id="div_infos_article">
                    <div class="flosrent_div" id="div_modify">
                        <form method="POST" action="../../controlers/admin/controler_admin.php?product=<?php echo $article['ID'];?>" enctype="multipart/form-data" id="form_add_article">
                        <h1 class="flosrent.txt">Modifier produit</h1>
                        <div id="div_name">
                            <label for="product_name" class="flosrent_label">Nom du produit : </label>
                            <input type="text" name = "product_name" class="flosrent_input" value="<?php echo $article['Nom'];?>">
                        </div>
                        <div id="div_description">
                            <label for="product_description" class="flosrent_label" id="label_description">Desription du produit : </label>
                            <textarea id="product_description" name = "product_description" class="flosrent_input" rows="6" cols="30"><?php echo $article['Description'];?></textarea>
                        </div>
                        <div id="div_prix">
                            <label for="product_price" class="flosrent_label">Prix du produit : </label>
                            <input type="text" name="product_price" class="flosrent_input" value="<?php echo $article['Prix'];?>">
                            <label for="product_price" class="flosrent_label">Points</label>
                        </div>
                        <div id="div_photo">
                            <label for="product_photo" class="flosrent_label">Photo du produit : </label>
                            <input type="file" name="product_photo" class="flosrent_input" placeholder="Parcourir">
                        </div>
                        <div id="div_stock">
                            <label for="product_stock" class="flosrent_label">Nombre en stock : </label>
                            <input type="text" name="product_stock" class="flosrent_input" value="<?php echo $article['Stock'];?>">
                        </div>
                        <input type="submit" class="flosrent_btn" name="btn_modify_product" value="Modifier" id="btn_add">
                        </form>
                    </div>


                    <div class="flosrent_div" id="div_historique">
                        <h1>Historique achats</h1>
                            <table class="flosrent_table" id="table_achats">
                                    <tr>
                                        <th class="flosrent_th">Pseudo</th>
                                        <th class="flosrent_th">Date d'achat</th>
                                    </tr>
                                        <?php    
                                            $switch = 0;
                                            while($achat = $get_achats->fetch())
                                            {
                                                $get_client = getClient($achat['Badge']);
                                                while($client = $get_client ->fetch())
                                                {
                                                    if($switch == 0)
                                                    { ?>
                                                        <tr class="flosrent_tr">
                                                            <td class="flosrent_td_white"><?php echo $client['Pseudo'];?></td>
                                                            <td class="flosrent_td_white"><p><?php echo $achat['Date_achat'];?></p></td>
                                                        </tr>
                                            <?php       $switch = 1;
                                                    }
                                                    elseif($switch == 1)
                                                    { ?>
                                                        <tr class="flosrent_tr">
                                                            <td class="flosrent_td_purple"><?php echo $client['Pseudo'];?></td>
                                                            <td class="flosrent_td_purple"><p></p><?php echo $achat['Date_achat'];?></p></td>
                                                        </tr>
                                            <?php       $switch = 0;
                                                    }
                                                }
                                                $get_client -> closeCursor();
                                            }
                                            $get_achats-> closeCursor();
                                        ?>    
                            </table>
                    </div>
                </div>
                <a href="../confirmation.php?type=5&client=&id=&product=<?php echo $_GET['product'];?>"><p class="flosrent_label" id="txt_delete">Supprimer le produit</p></a>
                </section>

<?php include("../footer.php"); ?>
</div>
</body>
</html>

<?php
}
else
{
header('Location: ../erreur.php?erreur=5');
}