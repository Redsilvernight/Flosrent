<?php
session_start();
require('../../controlers/verif_connection.php');

if(testConnection($_GET['tag']) == TRUE)
{
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/Nouveau_client.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Add clients</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Banniere_logo" id="img_banniere"></a>
            </header>

            <?php include("nav_admin.php"); ?>

            <section>
                <div class="flosrent_div" id="div_add">
                    <form id="form_identifiant" name="form_add_user"  method="post" action="../../controlers/admin/controler_admin.php?tag=<?php echo $_GET['tag'];?>">
                            <h1 class="flosrent_txt">Ajout client</h1><br>
                            <div id="div_prenom">
                            <label for="prenom_inscription" class="flosrent_label">Prenom<br></label>
                            <input type="text" name="prenom_inscription" class="flosrent_input" placeholder="Prenom">
                            </div>
                            <div id="div_nom">
                            <label for="nom_inscription" class="flosrent_label">Nom<br></label>
                            <input type="text" name="nom_inscription" class="flosrent_input" placeholder="Nom">
                            </div>
                            <div id="div_pseudo">
                                <label for="pseudoconnect" class="flosrent_label">Pseudo<br></label>
                                <input type="text" name="pseudoconnect" class="flosrent_input" placeholder="Pseudo" >
                            </div>
                            <div id="div_mdp">
                                <label for="mdpconnect" class="flosrent_label">Mot de passe<br></label>
                                <input type="password" name="mdpconnect" class="flosrent_input" placeholder="Mot de passe">
                            </div>
                            <div id="div_status">
                                <label>Status</label><br>
                                <input type="radio" id="radio_clients" name="status" value="Client">
                                <label for="radio_clients">Client</label>

                                <input type="radio" id="radio_admin" name="status" value="Admin">
                                <label for="radio_admin">Admin</label>
                            </div>
                            <div id="div_btn_login">
                                <input class="flosrent_btn" id="btn_add" type="submit" value="Ajouter" name="form_connection">
                            </div>
                    </form>
                </div>
            </section>

            <?php include("../footer.php"); ?>

        </div>
    </body>
</html>

<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}