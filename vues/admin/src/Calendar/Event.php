<?php
namespace Calendar;

class Event {

    private $id;

    private $start;

    public function getTag() {
        return $this->Badge;
    }

    public function getStart (): \DateTimeInterface {
        return new \DateTimeImmutable($this->Date_rdv);
    }

    public function getId () {
        return $this -> id;
    }

    public function setStart (string $start) {
        $this->Date_rdv = $start;
    }

    public function getState() {
        return $this->Etat;
    }

}
