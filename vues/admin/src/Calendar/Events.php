<?php
namespace Calendar;

class Events {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère les évènements commençant entre 2 dates
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @param mixed $tag
     * @return Event[]
     */
    public function getEventsBetween (\DateTimeInterface $start, \DateTimeInterface $end, $tag): array {
        $table = "rdv_".$tag;
        $sql = "SELECT * FROM ".$table." WHERE Date_rdv BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}' ORDER BY Date_rdv ASC";
        $statement = $this->pdo->query($sql);
        $statement->setFetchMode(\PDO::FETCH_CLASS, Event::class);
        $results = $statement->fetchAll();
        return $results;
    }
    /**
     * Récupère les évènements commençant entre 2 dates indexé par jour
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return array
     */
    public function getEventsBetweenByDay (\DateTimeInterface $start, \DateTimeInterface $end): array {
        $events = $this->getEventsBetween($start, $end, $_SESSION['tag']);
        $days = [];
        foreach($events as $event) {
            $date = $event->getStart()->format('Y-m-d');
            if (!isset($days[$date])) {
                $days[$date] = [$event];
            } else {
                $days[$date][] = $event;
            }
        }
        return $days;
    }

    /**
     * Récupère un évènement
     * @param int $id
     * @return Event
     * @throws \Exception
     */
    public function find (int $id): Event {
        $statement = $this->pdo->query("SELECT * FROM rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83 WHERE id = $id LIMIT 1");
        $statement->setFetchMode(\PDO::FETCH_CLASS, Event::class);
        $result = $statement->fetch();
        if ($result === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $result;
    }

    /**
     * @param Event $event
     * @param array $data
     * @return Event
     */
    public function hydrate (Event $event, array $data) {
        $event->setStart(\DateTimeImmutable::createFromFormat('Y-m-d H:i',
            $data['date'] . ' ' . $data['start'])->format('Y-m-d H:i:s'));
        return $event;
    }

    /**
     * Crée un évènement au niveau de la base de donnée
     * @param Event $event
     * @return bool
     */
    public function create (Event $event): bool {
        $statement = $this->pdo->prepare('INSERT INTO rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83 (Badge, Date_rdv) VALUES (?, ?)');
        return $statement->execute([
           2546,
           $event->getStart()->format('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Met à jour un évènement au niveau de la base de données
     * @param Event $event
     * @return bool
     */
    public function update (Event $event): bool {
        $statement = $this->pdo->prepare('UPDATE rdv_47c5c6f332aa2d927a70073a43090d99c39fcd83 SET Badge = ?, Date_rdv = ? WHERE id = ?');
        return $statement->execute([
            2546,
            $event->getStart()->format('Y-m-d H:i:s'),
            $event->getId()
        ]);
    }

    /**
     * TODO: Supprime un évènement
     * @param Event $event
     * @return bool
     */
    public function delete (Event $event): bool {

    }

}
