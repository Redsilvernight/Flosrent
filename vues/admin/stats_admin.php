<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/admin/menu_admin.css" rel="stylesheet">
        <link href="../../publics/css/admin/stats_admin.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Stats</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>
            
            <?php include("nav_admin.php"); ?>
            
            <section>
                <div id="div_top_stats" class="flosrent_div">
                    <h1>Top 3 clients</h1>
                </div>
                <div id="div_affluence" class="flosrent_div">
                    <h1>Affluences</h1>
                </div>
                <div id="div_top_boutique" class="flosrent_div">
                    <h1>Top 3 boutique</h1>
                </div>
                <div id="div_temps_total" class="flosrent_div">
                    <h1>Temps total</h1>
                </div>
            
            </section>
            
            <?php include("../footer.php"); ?>
        </div>
    </body>
</html>