<?php
session_start();
require('../controlers/verif_connection.php');
require('../controlers/gestion_confirm.php');


$confirmation = setConfirm($_GET['type'],$_GET['client'],$_GET['id'],$_GET['product']);

?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <link href="../publics/css/confirmation.css" rel="stylesheet">
        <title>Flo's Rent-Confirmation</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <section>
                <div class="flosrent_div" id="div_confirm">
                    <p class="flosrent_txt" id="txt_confirm"><?php echo $confirmation[0];?></p>
                    <div id="div_choice">
                        <a href="<?php echo $confirmation[1];?>"><p class="flosrent_label" id="txt_oui">Oui</p></a>
                        <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><p class="flosrent_label" id="txt_non">Non</p></a>
                    </div>
                    
                
                </div>
            
            
            
            </section>


            <?php include("footer.php"); ?>
        </div>
    </body>
</html>
