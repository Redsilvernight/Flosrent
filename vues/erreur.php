<?php
	$erreur_type = $_GET['erreur'];
	require("../controlers/gestion_erreurs.php");
	$erreur = afficheErreur($erreur_type);

?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
         <link href="../publics/css/admin/Nouveau_client.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Add clients</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Banniere_logo" id="img_banniere"></a>
            </header>
            
            <section>
                <div class="flosrent_div" id="div_add">
					
                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"> <div id="div_erreur">
						<p><?php echo $erreur ?></p>
					</div></a>
				</div>
            </section>

            <?php include("footer.php"); ?>

        </div>
    </body>
</html>
