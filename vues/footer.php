<footer class="flosrent_bg">
    <div id="div_img_secure">
        <img src = "/Flosrent/publics/Images/logo_secure.gif" alt="Site_securise" id="img_secure">
    </div>
    <p id="p_secure">Site plus ou moins sécurisé.</p>
    <div id="div_copyright">
        <img src="/Flosrent/publics/Images/copyright.gif" id="img_copyright" alt="Copyright" >
        <img src="/Flosrent/publics/Images/banniere.png" id="img_footer" alt="Image_banniere">
        <p id="p_date">2019-2020</p>
    </div>
    <div id="div_contact">
        <img src="/Flosrent/publics/Images/contact.png" id="img_contact" alt="img_contact">
        <a href="Contact.php"><p id="p_contact">Me contacter</p></a>
    </div>
</footer>
