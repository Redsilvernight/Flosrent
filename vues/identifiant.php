<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
        <link href="../publics/css/Identifiant.css" rel="stylesheet">
        <link href="../publics/css/Videos.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Identifiants</title>
    </head>
    
    <body>
        <div od="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Banniere_logo" id="img_banniere"></a>
            </header>
            
            <section>
                <div id="container_bg">
                    <video autoplay muted loop id="movie_bg">
                        <source src="../publics/Videos/CCTV.mp4" type="video/mp4">
                    </video>
                    <form id="form_identifiant" class="flosrent_bg" method="post" action="../controlers/connexion.php">
                        <h1 id="h1_connexion">Connexion</h1><br>
                        <div id="div_pseudo">
                            <label for="pseudoconnect" class="flosrent_label">Pseudo<br></label>
                            <input type="text" name="pseudoconnect" class="flosrent_input" placeholder="Pseudo" >
                        </div>
                        <div id="div_mdp">
                            <label for="mdpconnect" class="flosrent_label">Mot de passe<br></label>
                            <input type="password" name="mdpconnect" class="flosrent_input" placeholder="Mot de passe">
                        </div>
                        <div id="div_btn_login">
                            <input class="flosrent_btn" type="submit" value="Connexion" name="formconnexion">
                        </div>
                    </form>
                </div>
            </section>
            
            <?php include("footer.php"); ?>
        </div>
    </body>
</html>