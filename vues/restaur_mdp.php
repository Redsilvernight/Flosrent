<?php
session_start();
require('../controlers/verif_connection.php');
if(testConnection($_GET['tag']) == TRUE)
{



?>




<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../publics/css/header.css" rel="stylesheet">
        <link href="../publics/css/footer.css" rel="stylesheet">
        <link href="../publics/css/restaur_mdp.css" rel="stylesheet">
        <link href="../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Infos</title>
    </head>

    <body>
        <div id="div_main">
            <header>
                <a href="../routeur.php"><img src="../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>

            <section>
                <div>
                    <form method="post" action="../controlers/update_password.php?tag=<?php echo $_SESSION['tag'];?>" id="form_password" class="flosrent_bg">
                        <div class="div_password">
                            <label for="password" class="flosrent_label">Mot de passe actuel</label>
                            <input type="password" class="flosrent_input" name = "password" id="inp_password" placeholder="Mot de passe">
                        </div>
                        <div class="div_password">
                            <label for="new_password" class="flosrent_label">Nouveau mot de passe</label>
                            <input type="password" class="flosrent_input" name="new_password" id="inp_new_password" placeholder="Nouveau mot de passe">
                        </div>
                        <div class="div_password">
                            <label for="confirm_password" class="flosrent_label">Confirmer nouveau mot de passe</label>
                            <input type="password" class="flosrent_input" name="confirm_password" id="inp_confirm_password" placeholder="Nouveau mot de passe">
                        </div>
                        <div class="div_password">
                            <input type = "submit" value = "Confirmer" class="flosrent_input" id="btn_confirm" name="change_password">
                        </div>
                    </form>
                </div>
            </section>
            <?php include("footer.php"); ?>
        </div>
        
    </body>
</html>
<?php
}
