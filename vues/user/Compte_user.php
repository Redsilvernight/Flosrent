<?php
    session_start();
    require('../../controlers/verif_connection.php');
    
    require('../../controlers/user/controler_compte.php');

    if(testConnection($_GET['tag']) == TRUE)
    {
        $_SESSION['pseudo'] = getPseudo($_SESSION['tag']);
        $_SESSION['points'] = getPoints($_SESSION['tag']);

        $get_passages = getPassages($_SESSION['tag']);
        $total_point = CalculPointsTotal($_SESSION['tag']);
        $temps_passe = CalculTempsPasse($_SESSION['tag']);
        
        $get_all_rdv = getAllRdv($_SESSION['tag']);

        $get_achats = getAchats($_SESSION['tag']);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link href="../../publics/css/header.css" rel="stylesheet">
        <link href="../../publics/css/footer.css" rel="stylesheet">
        <link href="../../publics/css/user/menu_user.css" rel="stylesheet">
        <link href="../../publics/css/user/Compte_user.css" rel="stylesheet">
        <link href="../../publics/css/flosrent.css" rel="stylesheet">
        <title>Flo's Rent-Compte</title>
    </head>
    
    <body>
        <div id="div_main">
            <header>
                <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
            </header>
            
            <?php include("nav_user.php"); ?>
            
            <section>
                <div id="div_hello">
                    <h1 class="flosrent_txt">Bonjour <?php echo $_SESSION['pseudo'];?>.</h1>
                </div>

                <div id="div_center">
                    <div id="div_points" class="flosrent_div">
                        <h1>Vos points: <?php echo $_SESSION['points'];?></h1>
                    </div>

                    <div id="div_rdv" class="flosrent_div">
                        <?php
                        if($get_all_rdv->rowCount() > 0)
                        {
                            while($rdv = $get_all_rdv -> fetch())
                            {
                            ?>
                                <a href="../confirmation.php?type=4&client=&id=<?php echo $rdv['id'];?>" ><h1 class="flosrent_label" id="txt_rdv">RDV le <?php echo $rdv['Date_rdv'];?></h1></a>
                            <?php
                            }
                        }
                        else
                        {
                        ?>
                            <p>Vous n'avez aucun rendez-vous programmé.</p>
                        <?php
                        }
                        if($get_all_rdv->rowCount() < 3)
                        {
                        ?>
                        <a href="../../routeur.php?page=10"><p id="p_lien_reservation">Programmer un rendez-vous</p></a>
                        <?php
                        }
                        ?>
                    </div>

                    <div id="div_hist_achats" class="flosrent_div">
                        <h1>Historique achats</h1>
                    <table class="flosrent_table" id="table_achats">
                            <tr>
                                <th class="flosrent_th">Produits</th>
                                <th class="flosrent_th">Prix</th>
                                <th class="flosrent_th">Date d'achat</th>
                            </tr>
                                <?php    
                                    $switch = 0;
                                    while($achat = $get_achats->fetch())
                                    { 
                                        $get_article = getAchatsArticle($achat['Produit'],$_SESSION['tag']);
                                        while($article = $get_article->fetch())
                                        {
                                            if($switch == 0)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_white"><?php echo $article['Nom'];?></td>
                                                    <td class="flosrent_td_white"><p><?php echo $article['Prix'];?></p></td>
                                                    <td class="flosrent_td_white"><p></p><?php echo $achat['Date_achat'];?></p></td>
                                                </tr>
                                    <?php       $switch = 1;
                                            }
                                            elseif($switch == 1)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_purple"><?php echo $article['Nom'];?></td>
                                                    <td class="flosrent_td_purple"><p><?php echo $article['Prix'];?></p></td>
                                                    <td class="flosrent_td_purple"><p></p><?php echo $achat['Date_achat'];?></p></td>
                                                </tr>
                                    <?php       $switch = 0;
                                            }
                                        }
                                        $get_article -> closeCursor();
                                    }
                                    $get_achats-> closeCursor();
                                ?>    
                    </table>
                    </div>
                </div>

                <div id="div_temps_passe" class="flosrent_div">
                    <h1>Graphique temps passé</h1>
                </div>
                <div id="div_historique" class="flosrent_div">
                    <h1>Historique Passages</h1>
                    <table class="flosrent_table" id="table_historique">
                            <tr>
                                <th class="flosrent_th">Date d'entrée</th>
                                <th class="flosrent_th">Date de sortie</th>
                                <th class="flosrent_th">Points gagnés</th>
                            </tr>
                                <?php    
                                    $switch = 0;
                                    if($get_passages == TRUE)
                                    {
                                        while($passages = $get_passages->fetch())
                                        { 
                                            if($switch == 0)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_white"><?php echo $passages['Date_entree'];?></td>
                                                    <?php
                                                    if($passages['Date_sortie'] == '1111-11-11 11:11:11')
                                                    {
                                                    ?>
                                                        <td class="flosrent_td_white"><p>Annulé</p></td>
                                                        <td class="flosrent_td_white"><p>Annulé</p></td>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                    <td class="flosrent_td_white"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                    <td class="flosrent_td_white"><p></p><?php echo $passages['Points'];?></p></td>
                                                    <?php
                                                    }
                                                    ?>
                                                </tr>
                                                <?php   $switch = 1;
                                            }
                                            elseif($switch == 1)
                                            { ?>
                                                <tr class="flosrent_tr">
                                                    <td class="flosrent_td_purple"><?php echo $passages['Date_entree'];?></td>
                                                    <?php
                                                    if($passages['Date_sortie'] == '1111-11-11 11:11:11')
                                                    {
                                                    ?>
                                                        <td class="flosrent_td_purple"><p>Annulé</p></td>
                                                        <td class="flosrent_td_purple"><p>Annulé</p></td>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                        <td class="flosrent_td_purple"><p><?php echo $passages['Date_sortie'];?></p></td>
                                                        <td class="flosrent_td_purple"><p></p><?php echo $passages['Points'];?></p></td>
                                                    <?php
                                                    }
                                                    ?>
                                                </tr>
                                            <?php $switch = 0;
                                            }
                                        }
                                        $get_passages-> closeCursor();
                                    }
                                    else
                                    {

                                    } 
                                ?>    
                    </table>
                </div>
                <p id="p_total" class="flosrent_txt">Temps total passé :<br><?php echo $temps_passe;?> Heures.</p>
                <p id="p_points_total" class="flosrent_txt">Vous avez accumulé au total :<br><?php echo $total_point;?> points.</p>
                <a href="../../routeur.php?page=11&tag=<?php echo $_SESSION['tag'];?>"><p class="flosrent_btn" id="p_reset_mdp">Réinitialiser le mot de passe</p></a>
            
            </section>
            
            <?php include("../footer.php"); ?>
        </div>  
    </body>
</html>
<?php
    }
