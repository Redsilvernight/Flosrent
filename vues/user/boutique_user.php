<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/user/model_user.php');
    
if(testConnection($_GET['tag']) == TRUE)
{
    $get_boutique = getArticle($_SESSION['tag']);
    $points = getPoints($_SESSION['tag']);
?>
    <!DOCTYPE html>

    <html>
        <head>
            <meta charset="utf-8">
            <link href="../../publics/css/header.css" rel="stylesheet">
            <link href="../../publics/css/footer.css" rel="stylesheet">
            <link href="../../publics/css/user/menu_user.css" rel="stylesheet">
            <link href="../../publics/css/user/boutique_user.css" rel="stylesheet">
            <link href="../../publics/css/flosrent.css" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

            <title>Flo's Rent-Boutique</title>
        </head>
        
        <body>
            <div id="div_main">
                <header>
                    <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
                </header>
            
                <?php include("nav_user.php"); ?>

                <section>
                    <?php if (isset($_GET['success'])): ?>
                                <div class="container">
                                    <div class="alert alert-success">
                                        L'achat a bien était effectué
                                    </div>
                                </div>
                        <?php endif; ?>
                    <h1 class="flosrent_txt" id="p_points">Vos points: <?php echo $points;?></h1>
            
                    <div id="div_boutique" class="flosrent_bg">
                    <?php while($article = $get_boutique->fetch())
                    {
                        if($article['Stock'] == 0)
                        {?>
                            <div class="div_produit" id="div_stock_vide">
                                    <h1 id="h1_nom"><?php echo $article['Nom'];?></h1>
                                    <h3 id="h3_stock_mauvais"><?php echo $article['Stock'];?> en stock</h3>
                                    <img src="../../publics/Product_photos/<?php echo $article['Lien_photo'];?>" alt="img_article" width="400px" height="200px" id="photo_product">
                                    <div id="div_description">
                                        <p><?php echo $article['Description'];?></p>
                                    </div>
                                    <h2 id="h2_prix_mauvais"><?php echo $article['Prix'];?> Points</h2>
                                    <input type="submit" value="Acheter" class="flosrent_btn" name="btn_buy" id="btn_buy">
                                    <h2 id="h2_badge">n°<?php echo $article['Badge'];?></h2>
                            </div>
                    <?php
                        }

                        else
                        { ?>
                            <div class="div_produit" id="div_stock_dispo">
                                <form action="buy_article.php?tag=<?php echo $_SESSION['tag'];?>&article=<?php echo $article['ID'];?>" method="post" id="form_product">
                                    <h1 id="h1_nom"><?php echo $article['Nom'];?></h1>
                                    <?php if($article['Stock'] > 5)
                                    {
                                        if(intval($article['Stock']) == 99)
                                        {
                                            $article['Stock'] = "Une infinité";
                                        } ?>
                                        <h3 id="h3_stock_bon"><?php echo $article['Stock'];?> en stock</h3>
                                    <?php
                                    }
                                    else
                                    {?>
                                        <h3 id="h3_stock_mauvais"><?php echo $article['Stock'];?> en stock</h3>
                                    <?php
                                    }
                                    ?>
                                    <img src="../../publics/Product_photos/<?php echo $article['Lien_photo'];?>" alt="img_article" width="400px" height="200px" id="photo_product">
                                    <div id="div_description">
                                        <p><?php echo $article['Description'];?></p>
                                    </div>
                                    <h2 id="h2_points"><?php echo $article['Prix'];?> Points</h2>
                                    <input type="submit" value="Acheter" class="flosrent_btn" name="btn_buy" id="btn_buy">
                                    <h2 id="h2_badge">n°<?php echo $article['Badge'];?></h2>
                                </form>
                            </div>
                    <?php
                        }
                    }?>
                    </div>
                </session>

                <?php include("../footer.php"); ?>

            </div>
        </body>
    </html>
<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}