<?php
session_start();
require('../../controlers/verif_connection.php');
require('../../models/user/model_user.php');
    
if(testConnection($_GET['tag']) == TRUE)
{
    $article = getInfosArticle($_SESSION['tag'],$_GET['article'])->fetch();
?>
    <!DOCTYPE html>

    <html>
        <head>
            <meta charset="utf-8">
            <link href="../../publics/css/header.css" rel="stylesheet">
            <link href="../../publics/css/footer.css" rel="stylesheet">
            <link href="../../publics/css/flosrent.css" rel="stylesheet">
            <link href="../../publics/css/user/buy_article.css" rel="stylesheet">
            <title>Flo's Rent-Boutique</title>
        </head>
        
        <body>
            <div id="div_main">
                <header>
                    <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere"></a>
                </header>

                <section>
                    <div class="flosrent_div">
                        <img src="../../publics/Product_photos/<?php echo $article['Lien_photo'];?>" width="300px" height="200px" alt="image du produit" id="img_product">
                        <div id="div_description">
                            <p><?php echo $article['Description'];?></p>
                        </div>
                        <h1 class="flosrent_txt" id="h1_nom"><?php echo $article['Nom'];?></h1>
                        <p><br>-Vos Points : <?php echo getPoints($_SESSION['tag']);?></p>
                        <p>- Prix : <?php echo $article['Prix'];?></p>
                        <p>- Points restant après l'achat : <?php echo (floatval(getPoints($_SESSION['tag']))-floatval($article['Prix']));?></p>
                        <?php if(intval(getPoints($_SESSION['tag'])) >= intval($article['Prix']))
                        { ?>
                            <a href="../../routeur.php?page=18&tag=<?php echo $_SESSION['tag'];?>&article=<?php echo $article['ID'];?>"><p class="flosrent_btn">Acheter</p></a>
                        <?php
                        }
                        else
                        {?>
                            <p class="flosrent_error">Vous n'avez pas assez de points pour acheter cet article</p>
                        <?php
                        }
                        ?>
                        
                    </div>
                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><p class="flosrent_txt" id="p_retour">Retour</p></a>
                </section>

                <?php include("../footer.php"); ?>

            </div>
        </body>
    </html>
<?php
}
else
{
    header('Location: ../erreur.php?erreur=5');
}