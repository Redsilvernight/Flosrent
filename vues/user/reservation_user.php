<?php
session_start();
require('../../controlers/verif_connection.php');
    
if(testConnection($_GET['tag']) == TRUE)
{
?>

    <!DOCTYPE html>

    <html>
        <head>
            <meta charset="utf-8">
            <link href="../../publics/css/header.css" rel="stylesheet">
            <link href="../../publics/css/footer.css" rel="stylesheet">
            <link href="../../publics/css/user/menu_user.css" rel="stylesheet">
            <link href="../../publics/css/user/reservation_user.css" rel="stylesheet">
            <link href="../../publics/css/flosrent.css" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <title>Flo's Rent-Réserver</title>
        </head>
        
        <body>
            <div id="div_main">
                <header>
                    <a href="../../routeur.php"><img src="../../publics/Images/banniere.png" alt="Image_banniere" id="img_banniere" ></a>
                </header>
                
                <?php include("nav_user.php"); ?>
            
                <section>
                    <?php if (isset($_GET['success'])): ?>
                            <div class="container">
                                <div class="alert alert-success">
                                    Le rendez-vous a bien été enregistré
                                </div>
                            </div>
                    <?php endif; ?>
                    <div id="div_reservation" class="flosrent_div">
                        <h1>Prendre un rendez-vous</h1>
                        <form action="../../controlers/user/add_rdv.php" method="post" id="form_reservation">
                            <label for="inp_date" class="flosrent_label">Date:</label>
                            <input type="date" class="flosrent_input" name="inp_date">
                            <label for="inp_time" class="flosrent_label">Heure:</label>
                            <input type="time" class="flosrent_input" name="inp_time" value="16:20">
                            <input type="submit" name="btn_reservation" value="Reservez" id="btn_reservation" class="flosrent_btn">
                        </form>
                    </div>
                </section>
                
                <?php include("../footer.php"); ?>
            </div>
        </body>
    </html>
<?php
}
